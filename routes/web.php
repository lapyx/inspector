<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;


// Binds for route
Route::pattern('id', '\d+');

//Route::domain(env('APP_URL'))->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Auth::routes();

    Route::group(['middleware' => 'auth'], function () {
        Route::get('/', 'CPController@index')->name('cp');

        Route::get('/instruction', 'CPController@instruction')->name('instruction');
        Route::get('profile',           ['as' => 'profile',         'uses' => 'Auth\LoginController@profile']);
        Route::post('profile',          ['as' => 'profile_update',  'uses' => 'Auth\LoginController@profileUpdate']);

        // Пользователи
        MyRoute::resourcePermission('users', 'CP\UserController', [
            'show' => function () {
                Route::any('users/datatable', ['as' => 'users.datatables.data', 'uses' => 'CP\UserController@anyData']);
            }
        ]);

        // Менеджер файлов
        MyRoute::checkPermission('filemanager', [
            'show' => function () {
                Route::get('/filemanager', ['as' => 'filemanager.index', 'uses' => 'CPController@filemanager']);
            }
        ]);

        // Роли/Разрешения
        Route::group(['prefix' => 'rbac', 'as' => 'rbac.'], function () {
            MyRoute::resourcePermission('rbac.roles', 'CP\RBAC\RoleController', [
                'show' => function () {
                    Route::any('roles/datatable', ['as' => 'roles.datatables.data', 'uses' => 'CP\RBAC\RoleController@anyData']);
                }
            ]);

            MyRoute::resourcePermission('rbac.permissions', 'CP\RBAC\PermissionController', [
                'show' => function () {
                    Route::any('permissions/datatable', ['as' => 'permissions.datatables.data', 'uses' => 'CP\RBAC\PermissionController@anyData']);
                }
            ]);
        });

        // Инспекторы
        MyRoute::resourcePermission('inspectors', 'InspectorController', [
            'show' => function () {
                Route::any('inspectors/datatable', ['as' => 'inspectors.datatables.data', 'uses' => 'InspectorController@anyData']);
                Route::any('inspectors/excel', ['as' => 'inspectors.excel', 'uses' => 'InspectorController@getExcel']);
            },
            'edit' => function() {
                Route::post('inspectors/{inspector}/delete_file', ['as' => 'inspectors.delete_file', 'uses' => 'InspectorController@deleteFile']);
                Route::get('inspectors/{inspector}/readonly', ['as' => 'inspectors.readonly', 'uses' => 'InspectorController@readonly']);
            }
        ]);

        // Реестр Материалов
        MyRoute::resourcePermission('materials', 'MaterialController', [
            'show' => function () {
                Route::any('materials/datatable', ['as' => 'materials.datatables.data', 'uses' => 'MaterialController@anyData']);
                Route::any('materials/excel', ['as' => 'materials.excel', 'uses' => 'MaterialController@getExcel']);
            },
            'edit' => function() {
                Route::post('materials/{material}/delete_file', ['as' => 'materials.delete_file', 'uses' => 'MaterialController@deleteFile']);
                Route::get('materials/{material}/readonly', ['as' => 'materials.readonly', 'uses' => 'MaterialController@readonly']);
            }
        ]);

        // Реестр Мероприятий
        MyRoute::resourcePermission('reg_events', 'RegEventController', [
            'show' => function () {
                Route::any('reg_events/datatable', ['as' => 'reg_events.datatables.data', 'uses' => 'RegEventController@anyData']);
                Route::any('reg_events/excel', ['as' => 'reg_events.excel', 'uses' => 'RegEventController@getExcel']);
            },
            'edit' => function() {
                Route::post('reg_events/{reg_event}/delete_file', ['as' => 'reg_events.delete_file', 'uses' => 'RegEventController@deleteFile']);
                Route::get('reg_events/{reg_event}/readonly', ['as' => 'reg_events.readonly', 'uses' => 'RegEventController@readonly']);
            }
        ]);

        // Доступные роуты пользователя
        Route::group(['middleware' => 'role:admin'], function () {
            Route::any('inspectors/{inspector}/audit', ['as' => 'inspectors.audit', 'uses' => 'InspectorController@audit']);
            Route::any('materials/{material}/audit', ['as' => 'materials.audit', 'uses' => 'MaterialController@audit']);
            Route::any('reg_events/{reg_event}/audit', ['as' => 'reg_events.audit', 'uses' => 'RegEventController@audit']);

            Route::get('routes', function () {
                $routeCollection = Route::getRoutes();

                echo "<table style='width:100%'>";
                echo "<tr>";
                echo "<td width='10%'><h4>Name (as, path)</h4></td>";
                echo "<td width='50%'><h4>Corresponding Action</h4></td>";
                echo "<td width='10%'><h4>Methods</h4></td>";
                echo "<td width='30%'><h4>Uri (prefix, path)</h4></td>";
                echo "</tr>";
                foreach ($routeCollection as $value) {
                    echo "<tr>";
                    echo "<td>" . $value->getName() . "</td>";
                    echo "<td>" . $value->getActionName() . "</td>";
                    echo "<td>" . implode(', ', $value->methods()) . "</td>";
                    echo "<td>" . $value->uri() . "</td>";
                    echo "</tr>";
                }
                echo "</table>";
            });
        });
    });

    // Не правильное api не так надо делать
//    Route::group(['middleware' => 'api.token.verify', 'prefix' => 'api'], function () {
//        // Сохранение заявок
//        Route::post('orders/{usermib}', 'CP\Orders\JsonController@saveOrder');
//    });

//});
