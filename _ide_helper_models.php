<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Region
 *
 * @property int $id
 * @property string $name
 * @property string|null $code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereName($value)
 */
	class Region extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\UserRegion
 *
 * @property int $id
 * @property int $user_id
 * @property int $region_id
 * @property-read \App\Models\Region $region
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserRegion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserRegion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserRegion query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserRegion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserRegion whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserRegion whereUserId($value)
 */
	class UserRegion extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Thematic
 *
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Thematic newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Thematic newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Thematic query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Thematic whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Thematic whereName($value)
 */
	class Thematic extends \Eloquent {}
}

namespace App\Models{
/**
 * Class User
 *
 * @package App\Models
 * @mixin User
 * @property int $id
 * @property string $username
 * @property string $email
 * @property string|null $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RBAC\Permission[] $permissions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Region[] $regions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RBAC\Role[] $roles
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Session[] $sessions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePermissionIs($permission = '')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRoleIs($role = '')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsername($value)
 */
	class User extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Confirmation
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Confirmation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Confirmation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Confirmation query()
 */
	class Confirmation extends \Eloquent {}
}

namespace App\Models\RBAC{
/**
 * App\Models\RBAC\Role
 *
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RBAC\Permission[] $permissions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RBAC\Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RBAC\Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RBAC\Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RBAC\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RBAC\Role whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RBAC\Role whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RBAC\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RBAC\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RBAC\Role whereUpdatedAt($value)
 */
	class Role extends \Eloquent {}
}

namespace App\Models\RBAC{
/**
 * App\Models\RBAC\Permission
 *
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RBAC\Role[] $roles
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RBAC\Permission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RBAC\Permission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RBAC\Permission query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RBAC\Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RBAC\Permission whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RBAC\Permission whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RBAC\Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RBAC\Permission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RBAC\Permission whereUpdatedAt($value)
 */
	class Permission extends \Eloquent {}
}

namespace App\Models{
/**
 * Class RegEvent
 *
 * @package App\Models
 * @mixin RegEvent
 * @property int $id
 * @property string|null $name
 * @property int $inspector_id
 * @property int|null $region_id
 * @property array $images
 * @property \Illuminate\Support\Carbon|null $date_event Дата проведенного ОИ мероприятия по контролю
 * @property int $is_joint Совместное ?
 * @property int $is_control Надзорное мероприятие?
 * @property int $count_warnings Выявлено нарушений
 * @property float $sum_penaltys Наложено штрафов на сумму, тыс. руб.
 * @property string|null $description
 * @property string|null $notice Примечание
 * @property int $readonly Запрет редактирования
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read \App\Models\Inspector $inspector
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Material[] $materials
 * @property-read \App\Models\Region|null $region
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegEvent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegEvent newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegEvent onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegEvent query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegEvent whereCountWarnings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegEvent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegEvent whereDateEvent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegEvent whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegEvent whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegEvent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegEvent whereImages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegEvent whereInspectorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegEvent whereIsControl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegEvent whereIsJoint($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegEvent whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegEvent whereNotice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegEvent whereReadonly($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegEvent whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegEvent whereSumPenaltys($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegEvent whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegEvent withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RegEvent withoutTrashed()
 */
	class RegEvent extends \Eloquent {}
}

namespace App\Models{
/**
 * Class Material
 *
 * @package App\Models
 * @mixin Material
 * @property int $id
 * @property int|null $reg_event_id
 * @property int $inspector_id
 * @property int|null $thematic_id
 * @property int|null $region_id
 * @property array|null $images
 * @property \Illuminate\Support\Carbon|null $date_receipt Дата проведенного ОИ мероприятия по контролю
 * @property int $is_criminal Возбуждено дело на основании поступивших материалов ?
 * @property int $count_warnings Выявлено нарушений
 * @property int|null $confirmeted_warnings Подтверждено нарушений
 * @property int $count_protocols Составлено протоколов
 * @property int $count_cancel_protocols Отменено протоколов
 * @property float $sum_penaltys Наложено штрафов на сумму, тыс. руб.
 * @property string|null $description
 * @property string|null $notice Примечание
 * @property int $readonly Запрет редактирования
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read \App\Models\Confirmation $confirmation
 * @property-read \App\Models\Inspector $inspector
 * @property-read \App\Models\RegEvent|null $regEvent
 * @property-read \App\Models\Region|null $region
 * @property-read \App\Models\Thematic|null $thematic
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Material newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Material newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Material onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Material query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Material whereConfirmetedWarnings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Material whereCountCancelProtocols($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Material whereCountProtocols($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Material whereCountWarnings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Material whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Material whereDateReceipt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Material whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Material whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Material whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Material whereImages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Material whereInspectorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Material whereIsCriminal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Material whereNotice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Material whereReadonly($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Material whereRegEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Material whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Material whereSumPenaltys($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Material whereThematicId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Material whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Material withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Material withoutTrashed()
 */
	class Material extends \Eloquent {}
}

namespace App\Models{
/**
 * Class Inspector
 *
 * @package App\Models
 * @mixin Inspector
 * @property int $id
 * @property string|null $phone
 * @property string|null $email
 * @property string $fam
 * @property string $nam
 * @property string|null $otch
 * @property \Illuminate\Support\Carbon $birthday
 * @property string|null $placelive Место жительства
 * @property string|null $sex
 * @property int|null $region_id
 * @property string|null $notice Примечание
 * @property \Illuminate\Support\Carbon|null $date_statement1 Дата 1 подачи заявления
 * @property \Illuminate\Support\Carbon|null $date_statement3 Дата 3 подачи заявления
 * @property \Illuminate\Support\Carbon|null $date_statement2 Дата 2 подачи заявления
 * @property \Illuminate\Support\Carbon|null $doc_date_begin Дата выдачи удостоверения
 * @property \Illuminate\Support\Carbon|null $doc_date_end Дата сдачи удостоверения
 * @property \Illuminate\Support\Carbon|null $date_statement_doc Дата подачи заявления
 * @property \Illuminate\Support\Carbon|null $date_end_authority Дата прекращения полномочий
 * @property string|null $doc_number Номер удостоверения
 * @property int $readonly Запрет редактирования
 * @property int|null $user_id
 * @property string|null $public_org Членство в общественных организациях
 * @property string|null $job Место работы
 * @property \Illuminate\Support\Carbon|null $date_renewal3 Дата 3 продления
 * @property \Illuminate\Support\Carbon|null $date_renewal2 Дата 2 продления
 * @property \Illuminate\Support\Carbon|null $date_renewal1 Дата 1 продления
 * @property array $images
 * @property string|null $photo
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Material[] $materials
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RegEvent[] $regEvents
 * @property-read \App\Models\Region|null $region
 * @property-read \App\Models\User|null $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Inspector onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereDateEndAuthority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereDateRenewal1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereDateRenewal2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereDateRenewal3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereDateStatement1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereDateStatement2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereDateStatement3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereDateStatementDoc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereDocDateBegin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereDocDateEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereDocNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereFam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereImages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereJob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereNam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereNotice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereOtch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector wherePlacelive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector wherePublicOrg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereReadonly($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inspector whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Inspector withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Inspector withoutTrashed()
 */
	class Inspector extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Session
 *
 * @property int $id
 * @property string $token
 * @property int $user_id
 * @property string $user_agent
 * @property string $remote_ip
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $stoped_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session whereRemoteIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session whereStopedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session whereUserAgent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session whereUserId($value)
 */
	class Session extends \Eloquent {}
}

