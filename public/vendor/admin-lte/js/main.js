/* Popup Flash message begin */
function popupError(message, timesec) {
    popup(message, timesec, 'danger');
}

function popup(message, timesec, type) {
    type = type || 'success';
    var flash = $('#flashPopup');
    timesec = timesec ? timesec+'000' : 3000;
    if (!flash.length) {
        flash = $('<div id="flashPopup" class="flash-container flash-container-desktop ng-scope">');
        $('body').append(flash);
    }
    if (flash.find('.flash-message').length >= 10) {
        flash.find('.flash-message').first().remove();
    }
    var flashMessage = $('<div class="flash-message alert ng-binding ng-scope alert-'+type+'">');
    flashMessage.append('<button type="button" class="close" data-dismiss="alert">&times;</button>');
    flashMessage.append(message);
    flash.append(flashMessage);
    setTimeout(function() { flashMessage.remove(); }, timesec);
}
/* Popup Flash message end */

function ajaxSend(url, params, beforecallback, callback, errorcallback) {
    var submits = $('form').find('button, input[type=submit], input[type=button]');
    $.ajax({
        url: url,
        type: 'POST',
        data: params,
        dataType: 'json',
        beforeSend: function (data){
            if (typeof beforecallback == 'function')
                beforecallback(data);
            submits.prop('disabled', true);
        },
        success: function(data) {
            submits.prop('disabled', false);
            if (data.error != undefined) {
                popupError(data.error);
            } else {
                if (data.message != undefined)
                    popup(data.message);

                if (typeof callback == 'function')
                    callback(data);
            }
        },
        error: function(xhr, status, error){
            submits.prop('disabled', false);
            console.log(xhr.responseText);
            popupError('Ошибка ajax запроса, смотри консоль');
            if (typeof errorcallback == 'function')
                errorcallback(xhr, status, error);
        }
    });
}

function CopyToClipboard(copyText) {
  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  popup("Текст "+copyText.value+" скопирован", 5);
}

function initDeleteImages(url) {
    $('.upload-images').on('click', '.delete-file', function () {
        var btn = $(this);
        var img = btn.parent().find('img');
        $.ajax({
            type: 'post',
            url: url,
            data: {file: img.attr('src')},
            beforeSend: function () {
                btn.prop('disabled', true);
            },
            success: function (json) {
                btn.prop('disabled', false);
                if (json.error != null) {
                    popupError(json.error);
                } else if (json.message) {
                    popup(json.message);
                    btn.parent().remove();
                }
            },
            error: function () {
                btn.prop('disabled', false);
                popupError('Неизвестная ошибка, обратитесь к администратору');
            }
        });
    });
}

$(function(){
    $("#openFileManager").click(function(){
        $("#myModal").modal();
        if (! $("#myModal").find('iframe').attr('src')) {
            $("#myModal").find('iframe').attr('src', $("#myModal").find('iframe').attr('route'));
            $("#myModal").find('iframe').attr('route', '');
        }
    });

    $('.check_all').change(function () {
        $('input[type=checkbox][name*='+$(this).attr('v-name')+']').not('check_all').prop('checked', $(this).is(':checked'));
    })

    $('input[type=checkbox].icheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });

    $.datetimepicker.setLocale('ru');
    $('.datepicker').datetimepicker({
        i18n:{
            ru:{
                months:[
                    'Январь','Февраль','Март','Апрель',
                    'Май','Июнь','Июль','Август',
                    'Сентябрь','Октябрь','Ноябрь','Декабрь',
                ],
                dayOfWeek:[
                    "Вс", "Пн", "Вт", "Ср",
                    "Чт", "Пт", "Сб",
                ]
            }
        },
        timepicker:false,
        format:'d.m.Y',
        scrollInput : false
    });

    $('.select2').select2({
        placeholder: "Выберите вариант",
    });

    $(".select2add").select2({
        tags: "true",
        placeholder: "Выберите вариант",
        allowClear: true
    });

});
