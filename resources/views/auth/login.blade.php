@extends('auth.layouts.default')

@section('page-title', 'Login')

@section('css')

    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('vendor/icheck/skins/square/blue.css')}}">

@endsection

@section('content')

    <div class="login-box">
        <div class="login-logo" title="{{ config('app.fullname') }}">
            <div>
                <img src="{{ asset('i/eagle.png') }}" width="50px">
            </div>
            <a href="#">{{ config('app.name', 'Laravel') }}</a>
        </div>
        <!-- /.login-logo -->

        <div class="login-box-body">
            <p class="login-box-msg">@lang('cp.login_message')</p>
            <form action="{{ route('login') }}" method="POST">

                {{ csrf_field() }}

                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="@lang('cp.email')"
                           value="{{ old('email') }}" name="email"
                           required
                           autofocus>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="@lang('cp.password')"
                           name="password" required>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>

                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" name="remember"
                                       class="icheck" {{ old('remember') ? 'checked' : '' }}>
                                @lang('cp.remember_me')
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit"
                                class="btn btn-primary btn-block btn-flat">@lang('cp.sign_in')</button>
                    </div>
                    <!-- /.col -->
                </div>
                @if ($errors && $errors != '[]')
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $errors[0] }}
                    </div>
                @endif
            </form>

        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->

@endsection

@section('scripts')

    <!-- iCheck -->
    <script src="{{asset('vendor/icheck/icheck.min.js')}}"></script>

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>

@endsection