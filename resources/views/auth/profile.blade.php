@extends('cp.layouts.default')

@section('page-title', __('cp.my_profile'))

@section('page-header')

    <h1>@lang('cp.my_profile')</h1>

@endsection

@section('content')

    {{ Form::open(['route' => 'profile_update', 'method' => 'post']) }}

        {{ csrf_field() }}
        <div class="row">
            <div class="col-xs-12 col-sm-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @lang('cp.profile_name')
                        <a data-toggle="collapse" href="#profile" class="pull-right"><i class="fa fa-chevron-up"></i></a>
                    </div>

                    <div id="profile" class="panel-body collapse in">

                        <div class="form-group">
                            <label for="login">@lang('cp.profile.date_reg'):</label>
                            {{ $user->created_at?$user->created_at->format('d.m.Y H:i:s'):'Почему то пусто' }}
                        </div>
                        <div class="form-group">
                            <label for="username">@lang('cp.profile.username')</label>
                            {{ Form::input('text', 'username', $user->username, ['class' => 'form-control', 'id' => 'username', 'placeholder' => trans('cp.profile.username')]) }}
                        </div>
                        <div class="form-group">
                            <label for="email">@lang('cp.email')</label>
                            {{ Form::input('email', 'email', $user->email, ['class' => 'form-control', 'id' => 'email', 'placeholder' => trans('cp.email')]) }}
                        </div>
                        <div class="form-group">
                            <label>@lang('cp.regions_id'):</label>
                            {{ $user->getRegionsName() }}
                        </div>

                        <hr>
                        <h3>Смена пароля</h3>
                        <div class="form-group">
                            <label for="password">@lang('cp.password')</label>
                            <input type="password" id="password" name="password" placeholder="@lang('cp.password')"
                                   class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="password_confirmation">@lang('cp.password_confirmation')</label>
                            <input type="password" id="password_confirmation" name="password_confirmation"
                                   placeholder="@lang('cp.password_confirmation')"
                                   class="form-control">
                        </div>

                        <div class="form-group pull-right">
                            <button class="btn btn-primary">@lang('cp.save')</button>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        @lang('cp.roles')
                        <a data-toggle="collapse" href="#roles" class="pull-right"><i class="fa fa-chevron-up"></i></a>
                    </div>

                    <div id="roles" class="panel-body collapse in" style="padding-left: 0;">
                        <ul class="list-style-none">
                            @foreach($user->roles as $role)
                                <li>
                                    <a data-toggle="collapse" href="#{{ $role->name }}">{{ $role->display_name }}</a>
                                    <ul id="{{ $role->name }}" class="collapse">
                                        @foreach($role->permissions as $permission)
                                            <li>
                                                {{ $permission->display_name }}
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-8">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        Доступные роуты
                        <a data-toggle="collapse" href="#routes" class="pull-right"><i class="fa fa-chevron-up"></i></a>
                    </div>

                    <div id="routes" class="panel-body collapse in" style="padding-left: 0;">
                        <ul class="list-style-none">
                            @foreach($routes as $permissionName => $oneRoute)
                                <li>
                                    <a data-toggle="collapse" href="#perm_{{ $loop->index }}">{{ $loop->index+1 }}. {{ $permissionName!='all'?$permissionName:'Всем' }}</a>
                                    <ul class="list-style-none">
                                        <li>
                                            <table style='width:100%' id="perm_{{ $loop->index }}" class="collapse">
                                                <tr>
                                                    <th width='50%'>Uri (prefix, path)</th>
                                                    <th width='25%'>Name (as, path)</th>
                                                    <th width='25%'>Methods</th>
                                                </tr>
                                            @foreach($oneRoute as $route)
                                                <tr>
                                                    <td>{{ $route->uri() }}</td>
                                                    <td>{{ $route->getName() }}</td>
                                                    <td><small>({{ implode(', ', $route->methods() ) }})</small></td>
                                                </tr>
                                            @endforeach
                                            </table>
                                        </li>
                                    </ul>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        @lang('cp.permissions')
                        <a data-toggle="collapse" href="#permissions" class="pull-right"><i class="fa fa-chevron-up"></i></a>
                    </div>

                    <div id="permissions" class="panel-body collapse in" style="padding-left: 0;">
                        <ul>
                            @forelse($user->permissions as $permission)
                                <li>
                                    {{ $permission->display_name }}
                                </li>
                            @empty
                                Пусто
                            @endforelse
                        </ul>
                    </div>

                </div>
            </div>
        </div>

    {{ Form::close() }}
@endsection
