<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>@yield('page-title', 'ЕРОИ') - {{ config('app.name', 'Laravel') }}</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{ asset('vendor/datatables/css/dataTables.bootstrap.min.css') }}">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('vendor/admin-lte/css/AdminLTE.min.css') }}">

    <!-- AdminLTE Skins -->
    <link rel="stylesheet" href="{{ asset('vendor/admin-lte/css/skins/skin-green.min.css') }}">

    <link rel="stylesheet" href="{{ asset('vendor/admin-lte/css/main.css') }}">

    <link rel="stylesheet" href="{{ asset('vendor/datetimepicker/jquery.datetimepicker.min.css') }}">

    <link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css') }}">

    <link rel="stylesheet" href="{{ asset('vendor/fancybox/jquery.fancybox.css') }}">

    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('vendor/icheck/skins/square/blue.css')}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{ asset('vendor/html5shiv/html5shiv.min.js') }}"></script>
    <script src="{{ asset('vendor/respond.js/respond.min.js') }}"></script>
    <![endif]-->

    @yield('css')
</head>

<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">

@include('cp.parts.header')

    <div class="content-wrapper">

        <section class="content-header">

            @yield('page-header')

        </section>

        <section class="content">

            @include('cp.parts.message')

            @yield('content')

        </section>

    </div>

    @include('cp.parts.footer')

</div>

@include('cp.parts.main-sidebar')

</body>

<!-- jQuery -->
<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>

<!-- iCheck -->
<script src="{{asset('vendor/icheck/icheck.min.js')}}"></script>

<!-- DataTables TODO вынести в отдельный подключаемый шаблон -->
<script src="{{ asset('vendor/datatables/js/jquery.dataTables.bootstrap.min.js') }}"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>

<!-- AdminLTE App -->
<script src="{{ asset('vendor/admin-lte/js/app.min.js') }}"></script>

<script src="{{ asset('vendor/admin-lte/js/main.js?v1') }}"></script>

<script src="{{ asset('vendor/datetimepicker/jquery.datetimepicker.full.min.js') }}"></script>

<script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>

<script src="{{ asset('vendor/fancybox/jquery.fancybox.js') }}" type="text/javascript"></script>
@yield('scripts')

<script>
    $(document).ready(function () {
        $('.fancybox').fancybox();
    });
</script>

</html>
