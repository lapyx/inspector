@php
    if ($errors && is_object($errors)) {
        $has_errors = $errors->any();
        $errors = $errors->all();
    } else {
        $has_errors = count($errors) > 0;
    }

@endphp
@if ($has_errors)
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> @lang('cp.error')</h4>
        <ul>
            @foreach ($errors as $error)
                <li>{!! $error !!}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (\Session::has('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> @lang('cp.success')</h4>
        {!! \Session::get('success') !!}
    </div>
@endif
