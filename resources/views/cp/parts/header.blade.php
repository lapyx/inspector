<header class="main-header">

    <a href="{{ config('app.url') }}" class="logo" data-toggle="tooltip" data-placement="bottom" title="{{ config('app.fullname') }}">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
            <img src="{{ asset('i/eagle.png') }}" width="35px" alt="logo">
        </span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
            <img src="{{ asset('i/eagle.png') }}" width="35px" alt="logo">
            <b>{{ config('app.name') }}</b>
        </span>
    </a>

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="{{ route('cp') }}" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">@lang('cp.toggle_navigation')</span>
        </a>
        <ul class="nav navbar-nav navbar-right">
            {{--<li>--}}
                {{--<a href="{{ route('instruction') }}">Инструкция</a>--}}
            {{--</li>--}}
            <li class="dropdown banks-dropdown user-menu">
                <a href="#" data-toggle="dropdown" aria-expanded="true" class="dropdown-toggle">
                    <i class="fa fa-user-circle-o"></i>
                    <span>{{ $user->username }}</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-double-down pull-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <!-- The user image in the menu -->
                    <li class="user-header">
                        <p>
                            <span>{{ $user->username }}</span>
                            <small>Зарегистрирован: {{ $user->created_at->format('d.m.Y') }}</small>
                            <small>Регионы: <b>{{ $user->getRegionsName() }}</b></small>
                        </p>
                    </li>
                    <!-- Menu Body -->
                    {{--<li class="user-body">--}}
                        {{--<div class="col-xs-6 text-center mb10">--}}
                            {{--<a href="http://laraadmin.local/admin/lacodeeditor"><i class="fa fa-code"></i> <span>Editor</span></a>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="{{ route('profile') }}" class="btn btn-success btn-xs btn-flat text-white">
                                <i class="fa fa-user-circle-o"></i> @lang('cp.profile_name')
                            </a>
                        </div>
                        <div class="pull-right">
                            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-danger btn-xs btn-flat">
                                    <i class="fa fa-sign-out"></i> @lang('cp.sign_out')
                                </button>
                            </form>
                        </div>
                    </li>
                </ul>

            </li>
        </ul>
    </nav>

</header>
<style>
    .navbar-nav>.user-menu>.dropdown-menu>li.user-header {
        height: 100px !important;
    }
    .navbar-nav>.user-menu>.dropdown-menu>.user-footer {
        padding: 10px 25px !important;
    }
    .main-header .navbar-custom-menu a, .main-header .navbar-right a.btn-success {
        background: #00a65a !important;
    }
    @media (max-width: 767px) {
        .main-header .navbar-right {
            display: none;
        }
    }
</style>