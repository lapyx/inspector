@php
    if(!isset($action)){
        $action_name = request()->route()->getActionName();
        if($action_name) $action = explode('@', $action_name);
        if(isset($action[1])) $action = $action[1];
    }
    $query = '';
    if (isset($params) && !empty($params) && is_array($params)) {
        $query = '?'.http_build_query($params);
    }

    $readonly = isset($object) && method_exists($object, 'allowed') ? !$object->allowed($user, 'change') : false;
@endphp

@if($action == 'table')

    <form action="{{ $user->hasPermission(["$controller.destroy"]) ? route("$controller.destroy", $object) : '' }}" method="POST">

        <input type="hidden" name="_method" value="DELETE">

        {{ csrf_field() }}

        @if ($user->hasPermission(["$controller.show"]))
            <a href="{{ route("$controller.show", $object).$query }}" class="btn btn-info btn-xs" title="@lang('cp.show')">
                <i class="fa fa-eye"></i>
                {{--@lang('cp.show')--}}
            </a>
        @endif

        @if ($user->hasPermission(["$controller.edit"]) && !$readonly)
            <a href="{{ route("$controller.edit", $object).$query }}" class="btn btn-primary btn-xs" title="@lang('cp.edit')">
                <i class="fa fa-pencil"></i>
                {{--@lang('cp.edit')--}}
            </a>
        @endif

        @if ($user->hasPermission(["$controller.destroy"]) && !$readonly)
            <button onclick="return confirm('@lang('cp.confirm_destroy')')"
                    class="btn btn-danger btn-xs destroy-button"
                    title="@lang('cp.delete')">
                <i class="fa fa-trash"></i>
                {{--@lang('cp.delete')--}}
            </button>
        @endif

        @if (isset($object->readonly))
            @if ($user->hasRole('admin'))
                <a href="{{ route("$controller.readonly", $object).$query }}"
                        class="btn btn-warning btn-xs readonly-button"
                        data-toggle="tooltip"
                        title="@lang('cp.set_readonly.'.$object->readonly)">
                    <i class="fa fa-{{ $object->readonly ? 'lock' : 'unlock' }}" ></i>
                </a>
            @else
                <span class="pull-right"
                      data-toggle="tooltip"
                      title="@lang('cp.get_readonly.'.$object->readonly)">
                    <i class="fa fa-{{ $object->readonly ? 'lock' : 'unlock' }}" ></i>
                </span>
            @endif
        @endif

        @if (Route::has("$controller.audit") && $user->hasRole('admin'))
            <a href="{{ route("$controller.audit", $object).$query }}" class="btn btn-primary btn-xs" title="@lang('cp.audit')">
                <i class="fa fa-history"></i>
            </a>
        @endif
    </form>

@elseif($action == 'index')

    @if ($user->hasPermission(["$controller.create"]))
        <a href="{{ route("$controller.create") }}" class="btn btn-default btn-sm"><i
                    class="fa fa-plus"></i> @lang('cp.create_new')</a>
    @endif

    @if (Route::has("$controller.excel") && $user->hasRole('admin'))
        <a href="{{ route("$controller.excel") }}" class="btn btn-default btn-sm pull-right"><i
                    class="fa fa-file-excel-o"></i> @lang('cp.excel')</a>
    @endif

@elseif($action == 'create' && $user->hasPermission(["$controller.show"]))

    <a href="{{ route("$controller.index") }}" class="btn btn-default btn-sm"><i
                class="fa fa-list"></i> @lang('cp.return_to_index')</a>

@elseif($action == 'edit')

    <form action="{{ $user->hasPermission(["$controller.destroy"]) ? route("$controller.destroy", $object) : '' }}" method="POST">

        <input type="hidden" name="_method" value="DELETE">

        {{ csrf_field() }}

        @if ($user->hasPermission(["$controller.index"]))
            <a href="{{ route("$controller.index") }}" class="btn btn-default btn-sm"><i
                        class="fa fa-list"></i> @lang('cp.return_to_index')</a>
        @endif

        @if ($user->hasPermission(["$controller.show"]))
            <a href="{{ route("$controller.index") }}" class="btn btn-default btn-sm">
                <i class="fa fa-list"></i> @lang('cp.return_to_index')</a>
            <a href="{{ route("$controller.show", $object) }}" class="btn btn-default btn-sm">
                <i class="fa fa-eye"></i> @lang('cp.show')</a>
        @endif

        @if ($user->hasPermission(["$controller.create"]))
            <a href="{{ route("$controller.create") }}" class="btn btn-default btn-sm"><i
                    class="fa fa-plus"></i> @lang('cp.create_new')</a>
        @endif

        @if ($user->hasPermission(["$controller.destroy"]))
            <button onclick="return confirm('@lang('cp.confirm_destroy')')"
                    class="btn btn-default btn-sm destroy-button">
                <i class="fa fa-trash"></i> @lang('cp.delete')</button>
        @endif

    </form>

@elseif($action == 'show' || $action == 'audit')

    <form action="{{ $user->hasPermission(["$controller.destroy"]) ? route("$controller.destroy", $object) : '' }}" method="POST">

        <input type="hidden" name="_method" value="DELETE">

        {{ csrf_field() }}

        @if ($user->hasPermission(["$controller.show"]))
            <a href="{{ route("$controller.index") }}" class="btn btn-default btn-sm"><i
                        class="fa fa-list"></i> @lang('cp.return_to_index')</a>
        @endif

        @if ($user->hasPermission(["$controller.create"]))
            <a href="{{ route("$controller.create") }}" class="btn btn-default btn-sm"><i
                        class="fa fa-plus"></i> @lang('cp.create_new')</a>
        @endif

        @if ($user->hasPermission(["$controller.edit"]) && !$readonly)
            <a href="{{ route("$controller.edit", $object) }}" class="btn btn-default btn-sm"><i
                        class="fa fa-pencil"></i> @lang('cp.edit')</a>
        @endif

        @if ($user->hasPermission(["$controller.destroy"]) && !$readonly)
            <button onclick="return confirm('@lang('cp.confirm_destroy')')"
                    class="btn btn-default btn-sm destroy-button">
                <i class="fa fa-trash"></i> @lang('cp.delete')</button>
        @endif

        @if ($readonly)
            <span class="well well-sm text-danger pull-right" style="padding: 4px;margin: 0;">
                <strong>Редактирование/удаление</strong> этой записи вам запрещено.
            </span>
        @endif
    </form>

@endif
