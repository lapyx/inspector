<table id="table-{{ $table }}" class="table table-responsive table-bordered table-striped">
    <thead>
    <tr>
        @foreach($columns as $one)
            <th>@lang('cp.'.$one['data'])</th>
        @endforeach
    </tr>
    </thead>
</table>
@section('scripts')
<script>
$(function() {
    $('#table-{{ $table }}').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ $url }}',
        dom:"<'row bottom5'<'col-sm-12'<'pull-right'B>>>" +
            "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        buttons: [
            {
                extend: 'excel',
                text: '<i class="fa fa-file-excel-o"></i> Выгрузить таблицу в Excel',
                className: 'btn btn-default btn-sm'
            }
        ],
        language: {
            "url": "{{ asset('vendor/datatables/js/ru.lang') }}"
        },
        columns: JSON.parse('{!!  json_encode($columns, 1) !!}')
    });
});
</script>
@endsection
