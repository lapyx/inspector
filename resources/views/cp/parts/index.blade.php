@extends('cp.layouts.default')

@section('page-title', __('cp.'.$table))

@section('page-header')

    <h1>@lang('cp.'.$table)</h1>

@endsection

@section('content')

    <div class="panel panel-default">

        <div class="panel-body">

            @include('cp.parts.actions', ['controller' => $table])

        </div>

    </div>

    <div class="panel panel-default">

        <div class="panel-body">

            @include('cp.parts.table')

        </div>

    </div>

@endsection
