<ul>
    @foreach($menu as $page)

        @php $fullurl = array_get($page, 'link', $page['url']); @endphp
        <li page_fullurl="{{ $fullurl }}"
            page_url="{{ $page['url'] }}"
            page_id="{{ $page['id'] }}"
            page_active="{{ $page['active'] }}"
            page_sort="{{ $page['sort'] }}"
            page_header_menu="{{ $page['header_menu'] }}"
            @if (!$page['active']) data-jstree='{ "type" : "no_active" }'
            @elseif ($page['header_menu']) data-jstree='{ "type" : "in_header_menu" }' @endif
            >
            <a href="#">
                {{ $page['title'] }} <!-- {{ $page['id'] }} -->
                <span class="page-url italic-small text-muted">
                    @if ($fullurl) {{ $fullurl }} @endif
                </span>
            </a>
            @if (isset($page['pages']) && count($page['pages']) > 0)
                @include('cp.parts.menu-builder', [
                    'menu' => $page['pages']
                ])
            @endif
        </li>
    @endforeach
</ul>
