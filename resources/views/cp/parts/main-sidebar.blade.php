<aside class="main-sidebar">

    <section class="sidebar">

        <ul class="sidebar-menu">
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user-circle-o"></i>
                    <span>{{ Auth::user()->username }}</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-double-down pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="padding: 10px;">
                            {{ csrf_field() }}
                            <a href="{{ route('profile') }}" class="btn btn-success btn-xs btn-flat text-white">
                                <i class="fa fa-user-circle-o"></i> @lang('cp.profile_name')
                            </a>
                            <button type="submit" class="btn btn-danger btn-xs btn-flat">
                                <i class="fa fa-sign-out"></i> @lang('cp.sign_out')
                            </button>
                        </form>
                    </li>
                </ul>
            </li>
        </ul>

        <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="@lang('cp.search')">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form> -->

        <ul class="sidebar-menu">
            <li class="header">@lang('cp.navigation')</li>
            @foreach(config('menu') as $route => $nav)
                @php
                    $lang  = array_get($nav, 'lang', $route);
                    $faImg = array_get($nav, 'img');
                    $routeIndex = $route.'.index';
                    $active = preg_match('/^'.preg_quote($route).'/', Route::currentRouteName()) ? 'active' : '';
                    $badgeCount = '';
                @endphp
                @if (isset($nav['lists']))
                    @php
                        $lists = view('cp.parts.sidebar-lists', [
                            'lists' => $nav['lists'],
                            'route' => $route
                        ])->render();
                    @endphp
                    @if ($lists)
                        @if ($route == 'orders')
                            @php
                                $badgeCount = 0; //Example app(\App\Models\Order::class)->countNotAccepted();
                            @endphp
                        @endif
                        <li class="treeview {{$active}}">
                            <a href="#"><i class="fa {{$faImg}}"></i>
                                <span>@lang($lang) <span class="badge">{{ $badgeCount ? $badgeCount : '' }}</span></span>
                                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                            </a>
                            <ul class="treeview-menu">
                                {!! $lists !!}
                            </ul>
                        </li>
                    @endif
                @elseif($user->hasPermission($route.'.show'))
                    <li class="{{$active}}">
                        <a href="{{ route($routeIndex) }}"><i class="fa {{$faImg}}"></i>
                            <span>@lang($lang)</span>
                            <span class="badge pull-right label-info"></span>
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
    </section>

</aside>
