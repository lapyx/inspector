<!-- Modal -->
<div id="{{ $modalId }}" class="modal fade" role="dialog">
  <div class="modal-dialog{{ isset($modalClass) ? ' '.$modalClass : ''  }}">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ isset($title) ? $title : '' }}</h4>
      </div>
      <div class="modal-body">
        {{ isset($body) ? $body : '' }}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
