@foreach($lists as $child)
    @php
        $badgeCount = '';
        if (is_array($child)) {
            $childRoute = array_get($child, 0);
            $childLang  = array_get($child, 1);
        } else {
            $childRoute = $childLang  = $child;
        }

        if ($route == 'orders') {
            $badgeCount = app(\App\Models\Order::class)->countNotAccepted(\App\Enum\OrderTypeEnum::getByName($childRoute));
        }

        $childRoute = $route.'.'.$childRoute;
        $routeIndex = $childRoute.'.index';
    @endphp
    @if($user->hasPermission([$childRoute.'.show']))
        <li>
            <a href="{{ route($routeIndex) }}"><span>@lang($childLang) <span class="badge">{{ $badgeCount ? $badgeCount : '' }}</span></span></a>
        </li>
    @endif
@endforeach
