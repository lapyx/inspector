@foreach($model->images?:[] as $image)
    <div class="pull-left thumbnail" align="center">
        <div class="preview" data-toggle="tooltip" data-placement="top" title="{{ $image }}">
            {!! $model->getImg($image); !!}
        </div>
        @if (empty($hideDelete))
            <button type="button" class="btn btn-xs btn-danger delete-file">Удалить</button>
        @endif
    </div>
@endforeach