@extends('cp.layouts.default')

@section('page-title', __('cp.user')." $userEdit->username")

@section('page-header')

    <h1>@lang('cp.user')
        <small>{{ $userEdit->username }}</small>
    </h1>

@endsection

@section('content')

    <div class="panel panel-info">

        <div class="panel-body">

            @include('cp.parts.actions', ['controller'=>'users', 'object'=>$userEdit])

        </div>

    </div>

    <div class="panel panel-default">

        <div class="panel-body">

            <table class="table table-responsive table-bordered table-striped">
                <tbody>
                    <tr>
                        <td>@lang('cp.id')</td>
                        <td>{{ $userEdit->id }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.username')</td>
                        <td>{{ $userEdit->username }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.email')</td>
                        <td>{{ $userEdit->email }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.regions_id')</td>
                        <td>
                            {{ $userEdit->getRegionsName() }}
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('cp.created_at')</td>
                        <td>{{ $userEdit->created_at }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.updated_at')</td>
                        <td>{{ $userEdit->updated_at }}</td>
                    </tr>
                    {{--<tr>--}}
                        {{--<td>@lang('cp.user_lock')</td>--}}
                        {{--<td>{{ $userEdit->lock ? 'Да' : 'Нет' }}</td>--}}
                    {{--</tr>--}}
                </tbody>
            </table>

        </div>

    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-default">

                <div class="panel-heading">
                    @lang('cp.roles')
                </div>

                <div class="panel-body">
                    <ul>
                        @foreach($userEdit->roles()->orderBy('id', 'ASC')->get() as $role)
                            <li>
                                <a href="{{ action('CP\RBAC\RoleController@show', $role) }}">{{ $role->display_name }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>

            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-default">

                <div class="panel-heading">
                    @lang('cp.permissions')
                </div>

                <div class="panel-body">
                    <ul>
                        @foreach($userEdit->permissions()->orderBy('id', 'ASC')->get() as $permission)
                            <li>
                                <a href="{{ action('CP\RBAC\PermissionController@show', $permission) }}">{{ $permission->display_name }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>

            </div>
        </div>
    </div>

@endsection
