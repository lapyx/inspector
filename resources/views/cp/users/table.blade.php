<table id="table-users" class="table table-responsive table-bordered table-striped">
    <thead>
    <tr>
        <th>@lang('cp.id')</th>
        <th>@lang('cp.username')</th>
        <th>@lang('cp.email')</th>
        <th>@lang('cp.roles')</th>
        <th>@lang('cp.regions_id')</th>
        <th>@lang('cp.created_at')</th>
        <th>@lang('cp.table_actions')</th>
    </tr>
    </thead>
</table>
@section('scripts')
<script>
$(function() {
    $('#table-users').DataTable({
        processing: true,
        serverSide: true,
        language: {
            "url": "{{ asset('vendor/datatables/js/ru.lang') }}"
        },
        ajax: '{!! route('users.datatables.data') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'username', name: 'username' },
            { data: 'email', name: 'email' },
            { data: 'roles', name: 'roles' },
            { data: 'regions', name: 'regions' },
            { data: 'created_at', name: 'created_at' },
            // { data: 'lock', name: 'lock',
            //     "render": function ( data, type, row ) {
            //         if (data === "0") {
            //             return 'Вкл'
            //         } else {
            //             return 'Откл'
            //         }
            //     }
            // },
            { data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
});
</script>
@endsection
