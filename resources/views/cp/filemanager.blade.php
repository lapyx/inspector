@extends('cp.layouts.default')

@section('page-title', 'Control Panel')

@section('page-header')

    <h1>Page Header<small>Optional description</small></h1>

@endsection

@section('content')

    <iframe src="{{ route('unisharp.lfm.show') }}" style="width: 100%; height: 700px; overflow: hidden; border: none;"></iframe>

@endsection
