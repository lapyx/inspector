@extends('cp.layouts.default')

@section('page-title', __('cp.edit').": ".__('cp.role')." $role->name")

@section('page-header')

    <h1>@lang('cp.edit')
        <small>{!! $role->name !!}</small>
    </h1>

@endsection

@section('content')

    <div class="panel panel-default">

        <div class="panel-body">

            @include('cp.parts.actions', ['controller'=>'rbac.roles', 'object'=>$role])

        </div>

    </div>

    <form action="{{ action('CP\RBAC\RoleController@update', $role) }}" method="POST">

        <input type="hidden" name="_method" value="PUT">

        {{ csrf_field() }}

        <div class="panel panel-default">

            <div class="panel-body">

                <div class="form-group">
                    <label for="name">@lang('cp.name')</label>
                    <input type="text" id="name" name="name" value="{{ $role->name }}" placeholder="@lang('cp.name')"
                           class="form-control">
                </div>

                <div class="form-group">
                    <label for="display_name">@lang('cp.display_name')</label>
                    <input type="text" id="display_name" name="display_name" value="{{ $role->display_name }}"
                           placeholder="@lang('cp.display_name')"
                           class="form-control">
                </div>

                <div class="form-group">
                    <label for="description">@lang('cp.description')</label>
                    <textarea id="description" name="description" placeholder="@lang('cp.description')"
                              class="form-control">{{ $role->description }}</textarea>
                </div>

                <div class="form-group pull-right">
                    <button class="btn btn-primary">@lang('cp.save')</button>
                </div>


            </div>

        </div>


        <div class="panel panel-default">

            <div class="panel-heading">
                @lang('cp.permissions')
                <label class="pull-right">
                    <input type="checkbox" class="check_all" v-name="permissions"> Выбрать все:
                </label>
            </div>

            <div class="panel-body">

                <div class="form-group">
                    @foreach(app(\App\Models\RBAC\Permission::class)->getGroupPermissions() as $group)
                        @continue(!isset($group['lists']))
                        <ul class="list-style-none">
                            <li>
                                <h4>{{  $group['name'] }}</h4>
                                <ul class="list-style-none">
                                    @foreach($group['lists'] as $permission)
                                        @php
                                            $checked = $role->hasPermission($permission->name) ? ' checked': '';
                                        @endphp
                                        <li>
                                            <label><input type="checkbox" name="permissions[]" value="{{ $permission->id }}"
                                                          title="{{ $permission->display_name }}"{{ $checked }}> {{ $permission->display_name }}
                                            </label>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    @endforeach
                </div>

                <div class="form-group pull-right">
                    <button class="btn btn-primary">@lang('cp.save')</button>
                </div>

            </div>

        </div>

    </form>

@endsection
