<table id="table-permission" class="table table-responsive table-bordered table-striped">
    <thead>
    <tr>
        <th>@lang('cp.id')</th>
        <th>@lang('cp.name')</th>
        <th>@lang('cp.display_name')</th>
        <th>@lang('cp.description')</th>
        <th>@lang('cp.table_actions')</th>
    </tr>
    </thead>
</table>
@section('scripts')
<script>
$(function() {
    $('#table-permission').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('rbac.permissions.datatables.data') !!}',
        language: {
            "url": "{{ asset('vendor/datatables/js/ru.lang') }}"
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'display_name', name: 'display_name' },
            { data: 'description', name: 'description' },
            { data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
});
</script>
@endsection
