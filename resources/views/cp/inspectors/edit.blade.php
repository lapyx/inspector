@php /** @var \App\Models\Inspector $model */@endphp
@extends('cp.layouts.default')

@section('page-title', __('cp.edit').": ".__('cp.inspector')." ".$model->socrName())

@section('page-header')

    <h1>@lang('cp.edit')
        <small>{!! $model->socrName() !!}</small>
    </h1>

@endsection

@section('content')

    <div class="panel panel-default">

        <div class="panel-body">

            @include('cp.parts.actions', ['controller'=>'inspectors', 'object'=>$model])

        </div>

    </div>

    <form action="{{ route('inspectors.update', $model) }}" method="POST" enctype="multipart/form-data">

        <input type="hidden" name="_method" value="PUT">

        {{ csrf_field() }}

        <div class="panel panel-default">

            <div class="panel-body">

                @include('cp.inspectors.form', ['model' => $model])

                <div class="form-group pull-right">
                    <button class="btn btn-primary">@lang('cp.save')</button>
                </div>

            </div>

        </div>

    </form>

    @include('cp.inspectors.foreigns')

@endsection
