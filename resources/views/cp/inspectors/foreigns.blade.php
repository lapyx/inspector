<div class="row">
    <div class="col-xs-12 col-sm-6">
        <div class="panel panel-default">

            <div class="panel-heading">
                @lang('cp.materials')
            </div>

            <div class="panel-body">

                <div class="form-group">
                    @forelse($model->materials as $one)
                        {!! $one->getAhref() !!}
                        <br>
                    @empty
                        Пусто
                    @endforelse
                </div>

            </div>

        </div>
    </div>
    <div class="col-xs-12 col-sm-6">
        <div class="panel panel-default">

            <div class="panel-heading">
                @lang('cp.reg_events')
            </div>

            <div class="panel-body">

                <div class="form-group">
                    @forelse($model->regEvents as $one)
                        {!! $one->getAhref() !!}
                        <br>
                    @empty
                        Пусто
                    @endforelse
                </div>

            </div>

        </div>
    </div>
</div>