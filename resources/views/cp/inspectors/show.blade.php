@php /** @var $model \App\Models\Inspector */@endphp
@extends('cp.layouts.default')

@section('page-title', __('cp.inspector')." ".$model->socrName())

@section('page-header')

    <h1>@lang('cp.inspector')
        <small>{{ $model->socrName() }}</small>
    </h1>

@endsection

@section('content')

    <div class="panel panel-info">

        <div class="panel-body">

            @include('cp.parts.actions', ['controller'=>'inspectors', 'object'=>$model])

        </div>

    </div>

    <div class="panel panel-default">

        <div class="panel-body">

            <table class="table table-responsive table-bordered table-striped">
                <tbody>
                    <tr>
                        <td>@lang('cp.id')</td>
                        <td>{{ $model->id }}</td>
                        <td rowspan="4" align="center">{!! $model->getPhotoImg() !!}<br>@lang('cp.photo')</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.fam')</td>
                        <td>{{ $model->fam }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.nam')</td>
                        <td>{{ $model->nam }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.otch')</td>
                        <td>{{ $model->otch }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.birthday')</td>
                        <td colspan="2">{{ $model->birthday }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.sex')</td>
                        <td colspan="2">{{ $model->sex }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.region_id')</td>
                        <td colspan="2">{{ $model->region ? $model->region->name : '' }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.phone')</td>
                        <td colspan="2">{{ $model->phone }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.email')</td>
                        <td colspan="2">{{ $model->email }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.placelive')</td>
                        <td colspan="2">{{ $model->placelive }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.job')</td>
                        <td colspan="2">{{ $model->job }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.public_org')</td>
                        <td colspan="2">{{ $model->public_org }}</td>
                    </tr>
                    <tr>
                        <td colspan="2"><hr></td>
                    </tr>
                    <tr>
                        <td>@lang('cp.date_statement_doc')</td>
                        <td colspan="2">{{ $model->date_statement_doc }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.doc_date_begin')</td>
                        <td colspan="2">{{ $model->doc_date_begin }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.doc_number')</td>
                        <td colspan="2">{{ $model->doc_number }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.date_renewal')</td>
                        <td colspan="2">{{ $model->date_renewal1 }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.date_renewal')</td>
                        <td colspan="2">{{ $model->date_renewal2 }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.date_renewal')</td>
                        <td colspan="2">{{ $model->date_renewal3 }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.date_end_authority')</td>
                        <td colspan="2">{{ $model->date_end_authority }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.doc_date_end')</td>
                        <td colspan="2">{{ $model->doc_date_end }}</td>
                    </tr>

                    <tr>
                        <td>@lang('cp.date_statement1')</td>
                        <td colspan="2">{{ $model->date_statement1 }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.date_statement2')</td>
                        <td colspan="2">{{ $model->date_statement2 }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.date_statement3')</td>
                        <td colspan="2">{{ $model->date_statement3 }}</td>
                    </tr>

                    <tr>
                        <td>@lang('cp.notice')</td>
                        <td colspan="2">{{ $model->notice }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.created_at')</td>
                        <td colspan="2">{{ $model->created_at }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.registrator')</td>
                        <td colspan="2">{{ $model->user ? $model->user->socrName() : '' }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">@lang('cp.images')</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="upload-images">
                            @include('cp.parts.images-lists', ['hideDelete' => true])
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>

    </div>

    @include('cp.inspectors.foreigns')

@endsection
