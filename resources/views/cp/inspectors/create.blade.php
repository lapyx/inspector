@extends('cp.layouts.default')

@section('page-title', __('cp.create').": ".__('cp.inspector'))

@section('page-header')

    <h1>@lang('cp.create')</h1>

@endsection

@section('content')

    <div class="panel panel-default">

        <div class="panel-body">

            @include('cp.parts.actions', ['controller'=>'inspectors'])

        </div>

    </div>

    <form action="{{ action('InspectorController@store') }}" method="POST" enctype="multipart/form-data">

        {{ csrf_field() }}

        <div class="panel panel-default">

            <div class="panel-body">

                @include('cp.inspectors.form')

                <div class="form-group pull-right">
                    <button class="btn btn-primary">@lang('cp.create')</button>
                </div>

            </div>

        </div>

    </form>


@endsection
