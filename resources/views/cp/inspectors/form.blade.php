@php $model = isset($model) ? $model : new \App\Models\Inspector() @endphp

<div class="row">
    <div class="col-sm-6 form-face-photo" align="center">
        <div class="form-group" align="center">
            <div class="face-photo">
                {!! $model->getPhotoImg() !!}
            </div>
            <input type="file" name="photo" >
            <label>@lang('cp.photo')</label>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label for="fam">@lang('cp.fam')</label>
            <input type="text" id="fam" name="fam" placeholder="@lang('cp.fam')" value="{{ old('fam', $model->fam) }}"
                   class="form-control">
        </div>

        <div class="form-group">
            <label for="nam">@lang('cp.nam')</label>
            <input type="text" id="nam" name="nam" placeholder="@lang('cp.nam')" value="{{ old('nam', $model->nam) }}"
                   class="form-control">
        </div>

        <div class="form-group">
            <label for="otch">@lang('cp.otch')</label>
            <input type="text" id="otch" name="otch" placeholder="@lang('cp.otch')" value="{{ old('otch', $model->otch) }}"
                   class="form-control">
        </div>

        <div class="form-group">
            <label for="birthday">@lang('cp.birthday')</label>
            <input type="text" id="birthday" name="birthday" placeholder="@lang('cp.birthday')" value="{{ old('birthday', $model->birthday) }}"
                   class="form-control datepicker">
        </div>
    </div>
</div>
<div class="form-group">
    <label for="sex">@lang('cp.sex')</label>
    <select id="sex" name="sex" class="form-control">
        <option @if(old('sex', $model->sex) == 'M') selected @endif value="M">Мужской</option>
        <option @if(old('sex', $model->sex) == 'F') selected @endif value="F">Женский</option>
    </select>
</div>
<div class="form-group">
    <label for="region_id">@lang('cp.region_id')</label>
    <select id="region_id" name="region_id" class="form-control select2">
        <option></option>
        @foreach($regions as $region)
            <option @if(old('region_id', $model->region_id) == $region->id) selected @endif value="{{ $region->id }}">{{ $region->name }}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="otch">@lang('cp.phone')</label>
    <input type="text" id="phone" name="phone" placeholder="@lang('cp.phone')" value="{{ old('phone', $model->phone) }}"
           class="form-control">
</div>
<div class="form-group">
    <label for="otch">@lang('cp.email')</label>
    <input type="email" id="email" name="email" placeholder="@lang('cp.email')" value="{{ old('email', $model->email) }}"
           class="form-control">
</div>
<div class="form-group">
    <label for="placelive">@lang('cp.placelive')</label>
    <input type="text" id="placelive" name="placelive" placeholder="@lang('cp.placelive')" value="{{ old('placelive', $model->placelive) }}"
           class="form-control">
</div>
<div class="form-group">
    <label for="job">@lang('cp.job')</label>
    <input type="text" id="job" name="job" placeholder="@lang('cp.job')" value="{{ old('job', $model->job) }}"
           class="form-control">
</div>
<div class="form-group">
    <label for="public_org">@lang('cp.public_org')</label>
    <input type="text" id="public_org" name="public_org" placeholder="@lang('cp.public_org')" value="{{ old('public_org', $model->public_org) }}"
           class="form-control">
</div>

<hr>

<div class="form-group">
    <label for="date_statement_doc">@lang('cp.date_statement_doc')</label>
    <input type="text" id="date_statement_doc" name="date_statement_doc" placeholder="@lang('cp.date_statement_doc')" value="{{ old('date_statement_doc', $model->date_statement_doc) }}"
           class="form-control datepicker">
</div>
<div class="form-group">
    <label for="doc_date_begin">@lang('cp.doc_date_begin')</label>
    <input type="text" id="doc_date_begin" name="doc_date_begin" placeholder="@lang('cp.doc_date_begin')" value="{{ old('doc_date_begin', $model->doc_date_begin) }}"
           class="form-control datepicker">
</div>
<div class="form-group">
    <label for="doc_number">@lang('cp.doc_number')</label>
    <input type="text" id="doc_number" name="doc_number" placeholder="@lang('cp.doc_number')" value="{{ old('doc_number', $model->doc_number) }}"
           class="form-control">
</div>
<div class="form-group">
    <label for="date_statement">@lang('cp.date_statement1')</label>
    <input type="text" id="date_statement1" name="date_statement1" placeholder="@lang('cp.date_statement1')" value="{{ old('date_statement1', $model->date_statement1) }}"
           class="form-control datepicker">
</div>
<div class="form-group">
    <label for="date_statement">@lang('cp.date_renewal')</label>
    <input type="text" id="date_renewal1" name="date_renewal1" placeholder="@lang('cp.date_renewal')" value="{{ old('date_renewal1', $model->date_renewal1) }}"
           class="form-control datepicker">
</div>
<div class="form-group">
    <label for="date_statement">@lang('cp.date_statement2')</label>
    <input type="text" id="date_statement2" name="date_statement2" placeholder="@lang('cp.date_statement2')" value="{{ old('date_statement2', $model->date_statement2) }}"
           class="form-control datepicker">
</div>
<div class="form-group">
    <label for="date_statement">@lang('cp.date_renewal')</label>
    <input type="text" id="date_renewal2" name="date_renewal2" placeholder="@lang('cp.date_renewal')" value="{{ old('date_renewal2', $model->date_renewal2) }}"
           class="form-control datepicker">
</div>
<div class="form-group">
    <label for="date_statement">@lang('cp.date_statement3')</label>
    <input type="text" id="date_statement3" name="date_statement3" placeholder="@lang('cp.date_statement3')" value="{{ old('date_statement3', $model->date_statement3) }}"
           class="form-control datepicker">
</div>
<div class="form-group">
    <label for="date_statement">@lang('cp.date_renewal')</label>
    <input type="text" id="date_renewal3" name="date_renewal3" placeholder="@lang('cp.date_renewal')" value="{{ old('date_renewal3', $model->date_renewal3) }}"
           class="form-control datepicker">
</div>


<div class="form-group">
    <label for="date_end_authority">@lang('cp.date_end_authority')</label>
    <input type="text" id="date_end_authority" name="date_end_authority" placeholder="@lang('cp.date_end_authority')" value="{{ old('date_end_authority', $model->date_end_authority) }}"
           class="form-control datepicker">
</div>
<div class="form-group">
    <label for="doc_date_end">@lang('cp.doc_date_end')</label>
    <input type="text" id="doc_date_end" name="doc_date_end" placeholder="@lang('cp.doc_date_end')" value="{{ old('doc_date_end', $model->doc_date_end) }}"
           class="form-control datepicker">
</div>

<div class="form-group">
    <label for="notice">@lang('cp.notice')</label>
    <textarea id="notice" name="notice" placeholder="@lang('cp.notice')" class="form-control">{{ old('notice', $model->notice) }}</textarea>
</div>

<div class="form-group">
    <label for="images">@lang('cp.images')</label>
    <input type="file" id="images" name="images[]" multiple placeholder="@lang('cp.images')"
           class="form-control">

    @if ($model->id)
        <hr>
        <div class="upload-images">
            @include('cp.parts.images-lists')
        </div>
    @section('scripts')
        <script>
            $(function () {
                initDeleteImages("{{ route('inspectors.delete_file', $model) }}");
            });
        </script>
    @endsection
    @endif
</div>