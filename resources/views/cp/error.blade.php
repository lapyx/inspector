@extends('cp.layouts.default')

@section('page-title', 'Error '.$exception)

@section('page-header')

    <h1>Error {{ $exception }}</h1>

@endsection

@section('content')

    <font color="red">{!! $message !!}</font>

@endsection
