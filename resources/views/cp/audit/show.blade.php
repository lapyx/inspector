@php /** @var \Spatie\Activitylog\Models\Activity[] $activites*/ @endphp
@extends('cp.layouts.default')

@section('page-title', __('cp.audit').": ".__('cp.inspector'))

@section('page-header')

    <h1>@lang('cp.audit')</h1>

@endsection

@section('content')

    <div class="panel panel-default">

        <div class="panel-body">

            @include('cp.parts.actions', ['controller'=>'inspectors', 'object'=>$model])

        </div>

    </div>

    <div class="panel panel-default">

        <div class="panel-body">

            <div role="tabpanel" class="tab-pane fade in p20 bg-white" id="tab-timeline">
                <ul class="timeline timeline-inverse">
                    @foreach($activites as $one)
                        @php
                            $changes = $one->changes();
                            $attributes = isset($changes['attributes'])
                                ? array_unset_keys($changes['attributes'], ['updated_at', 'readonly'])
                                : [];
                            $old_attributes = isset($changes['old'])? $changes['old']: [];
                        @endphp
                        @if ($attributes)
                            <!-- timeline time label -->
                            <li class="time-label">
                                <span class="bg-{{ $one->description == 'deleted' ? 'red' : 'green' }}">
                                    <small>{{ $one->created_at->format('d.m.Y') }}
                                    (@lang('cp.'.$one->description))</small>
                                </span>
                            </li>
                            <!-- /.timeline-label -->
                            <!-- timeline item -->
                            <li>
                                @if ($one->description == 'deleted')
                                    <i class="fa fa-trash bg-blue"></i>
                                @else
                                    <i class="fa fa-pencil bg-blue"></i>
                                @endif

                                <div class="timeline-item">
                                    <span class="time"><i class="fa fa-clock-o"></i> {{ $one->created_at->format('H:i') }}</span>

                                    <h3 class="timeline-header">@lang('cp.'.$one->description) записи</h3>

                                    <div class="timeline-body" style="background: white;">
                                        <table class="table table-condensed">
                                            <thead>
                                            <tr>
                                                <th>Поле</th>
                                                @if ($one->description == 'updated')
                                                    <th>Старое значение</th>
                                                @endif
                                                <th>Новое значение</th>
                                            </tr>
                                            </thead>
                                            @php
                                                $attributes = convert_fields_by_model_name($one, $attributes);
                                                $old_attributes = convert_fields_by_model_name($one, $old_attributes);
                                            @endphp
                                            @foreach($attributes as $key => $new)
                                                @if (isset($old_attributes) && $one->description == 'updated')
                                                    @php
                                                        $old = array_get($old_attributes, $key);
                                                    @endphp
                                                    <tr>
                                                        <td>@lang("cp.$key")</td>
                                                        <td>{!! is_array($old) ? implode(',', $old) : $old !!}</td>
                                                        <td>{!! is_array($new) ? implode(',', $new) : $new !!}</td>
                                                    </tr>
                                                @elseif($one->description != 'updated')
                                                    <tr>
                                                        <td>@lang("cp.$key")</td>
                                                        <td>{!! is_array($new) ? implode(',', $new) : $new !!}</td>
                                                    </tr>
                                                @endif
                                            @endforeach

                                        </table>
                                    </div>
                                </div>
                            </li>
                        @endif
                        <!-- END timeline item -->
                    @endforeach
                    {{--<!-- timeline item -->--}}
                    {{--<li>--}}
                        {{--<i class="fa fa-user bg-aqua"></i>--}}

                        {{--<div class="timeline-item">--}}
                            {{--<span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span>--}}

                            {{--<h3 class="timeline-header no-border"><a href="#">Sarah Young</a> accepted your friend request--}}
                            {{--</h3>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<!-- END timeline item -->--}}
                    {{--<!-- timeline item -->--}}
                    {{--<li>--}}
                        {{--<i class="fa fa-comments bg-yellow"></i>--}}

                        {{--<div class="timeline-item">--}}
                            {{--<span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>--}}

                            {{--<h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>--}}

                            {{--<div class="timeline-body">--}}
                                {{--Take me to your leader!--}}
                                {{--Switzerland is small and neutral!--}}
                                {{--We are more like Germany, ambitious and misunderstood!--}}
                            {{--</div>--}}
                            {{--<div class="timeline-footer">--}}
                                {{--<a class="btn btn-warning btn-flat btn-xs">View comment</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<!-- END timeline item -->--}}
                    {{--<!-- timeline time label -->--}}
                    {{--<li class="time-label">--}}
					{{--<span class="bg-green">--}}
						{{--3 Jan. 2014--}}
					{{--</span>--}}
                    {{--</li>--}}
                    {{--<!-- /.timeline-label -->--}}
                    {{--<!-- timeline item -->--}}
                    {{--<li>--}}
                        {{--<i class="fa fa-camera bg-purple"></i>--}}

                        {{--<div class="timeline-item">--}}
                            {{--<span class="time"><i class="fa fa-clock-o"></i> 2 days ago</span>--}}

                            {{--<h3 class="timeline-header"><a href="#">Mina Lee</a> uploaded new photos</h3>--}}

                            {{--<div class="timeline-body">--}}
                                {{--<img src="http://placehold.it/150x100" alt="..." class="margin">--}}
                                {{--<img src="http://placehold.it/150x100" alt="..." class="margin">--}}
                                {{--<img src="http://placehold.it/150x100" alt="..." class="margin">--}}
                                {{--<img src="http://placehold.it/150x100" alt="..." class="margin">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<!-- END timeline item -->--}}
                    {{--<li>--}}
                        {{--<i class="fa fa-clock-o bg-gray"></i>--}}
                    {{--</li>--}}
                </ul>
                <!--<div class="text-center p30"><i class="fa fa-list-alt" style="font-size: 100px;"></i> <br> No posts to show</div>-->
            </div>

        </div>

    </div>


@endsection