@php $model = isset($model) ? $model : new \App\Models\RegEvent() @endphp

<div class="form-group">
    <label for="name">@lang('cp.name')</label>
    <input type="text" id="name" name="name" placeholder="@lang('cp.name')" value="{{ old('name', $model->name) }}"
           class="form-control">
</div>

<div class="form-group">
    <label for="inspector_id">@lang('cp.inspector')</label>
    <select id="inspector_id" name="inspector_id" class="form-control select2">
        <option></option>
        @foreach($inspectors as $inspector)
            <option @if(old('inspector_id', $model->inspector_id) == $inspector->id) selected @endif value="{{ $inspector->id }}">{{ $inspector->socrName() }} ({{ $inspector->region->name }})</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="region_id">@lang('cp.region_id')</label>
    <select id="region_id" name="region_id" class="form-control select2">
        <option></option>
        @foreach($regions as $region)
            <option @if(old('region_id', $model->region_id) == $region->id) selected @endif value="{{ $region->id }}">{{ $region->name }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="date_event">@lang('cp.date_event')</label>
    <input type="text" id="date_event" name="date_event" placeholder="@lang('cp.date_event')" value="{{ old('date_event', $model->date_event) }}"
           class="form-control datepicker">
</div>

<div class="checkbox icheck">
    <label>
        <input type="checkbox" name="is_joint"
               class="icheck" {{ old('is_joint', $model->is_joint) ? 'checked' : '' }} value="1">
        @lang('cp.is_joint')
    </label>
</div>

<div class="checkbox icheck">
    <label>
        <input type="checkbox" name="is_control"
               class="icheck" {{ old('is_control', $model->is_control) ? 'checked' : '' }} value="1">
        @lang('cp.is_control')
    </label>
</div>

<div class="form-group">
    <label for="count_warnings">@lang('cp.count_warnings')</label>
    <input type="text" id="count_warnings" name="count_warnings" placeholder="@lang('cp.count_warnings')" value="{{ old('count_warnings', $model->count_warnings) }}"
           class="form-control">
</div>

<div class="form-group">
    <label for="sum_penaltys">@lang('cp.sum_penaltys')</label>
    <input type="text" id="sum_penaltys" name="sum_penaltys" placeholder="@lang('cp.sum_penaltys')" value="{{ old('sum_penaltys', $model->sum_penaltys) }}"
           class="form-control">
</div>

<div class="form-group">
    <label for="description">@lang('cp.description')</label>
    <textarea id="description" name="description" placeholder="@lang('cp.description')" class="form-control">{{ old('description', $model->description) }}</textarea>
</div>

<div class="form-group">
    <label for="notice">@lang('cp.notice')</label>
    <textarea id="notice" name="notice" placeholder="@lang('cp.notice')" class="form-control">{{ old('notice', $model->notice) }}</textarea>
</div>

<div class="form-group">
    <label for="images">@lang('cp.images')</label>
    <input type="file" id="images" name="images[]" multiple placeholder="@lang('cp.images')"
           class="form-control">

    @if ($model->id)
        <hr>
        <div class="upload-images">
            @include('cp.parts.images-lists')
        </div>
    @section('scripts')
        <script>
            $(function () {
                initDeleteImages("{{ route('reg_events.delete_file', $model) }}");
            });
        </script>
    @endsection
    @endif
</div>