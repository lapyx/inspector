<div class="panel panel-default">

    <div class="panel-heading">
        Материалы
    </div>

    <div class="panel-body">

        <div class="form-group">
            @forelse($model->materials as $one)
                {!! $one->getAhref() !!}
                <br>
            @empty
                Пусто
            @endforelse
        </div>

    </div>

</div>