@php /** @var $model \App\Models\RegEvent */@endphp
@extends('cp.layouts.default')

@section('page-title', __('cp.material')." ".$model->id)

@section('page-header')

    <h1>@lang('cp.reg_event')
        <small>ID: {{ $model->id }}</small>
    </h1>

@endsection

@section('content')

    <div class="panel panel-info">

        <div class="panel-body">

            @include('cp.parts.actions', ['controller'=>'reg_events', 'object'=>$model])

        </div>

    </div>

    <div class="panel panel-default">

        <div class="panel-body">

            <table class="table table-responsive table-bordered table-striped">
                <tbody>
                    <tr>
                        <td>@lang('cp.id')</td>
                        <td>{{ $model->id }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.name')</td>
                        <td>{{ $model->name }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.inspector')</td>
                        <td>{{ $model->inspector->socrName() }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.region_id')</td>
                        <td colspan="2">{{ $model->region ? $model->region->name : '' }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.date_event')</td>
                        <td>{{ $model->date_event }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.is_joint')</td>
                        <td>{{ $model->is_joint ? 'Да' : 'Нет' }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.is_control')</td>
                        <td>{{ $model->is_control ? 'Да' : 'Нет' }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.count_warnings')</td>
                        <td>{{ $model->count_warnings }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.sum_penaltys')</td>
                        <td>{{ $model->sum_penaltys }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.description')</td>
                        <td>{{ $model->description }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.notice')</td>
                        <td>{{ $model->notice }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cp.created_at')</td>
                        <td>{{ $model->created_at }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">@lang('cp.images')</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="upload-images">
                            @include('cp.parts.images-lists', ['hideDelete' => true])
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>

    </div>

    @include('cp.reg_events.foreigns')

@endsection
