@php $model = isset($model) ? $model : new \App\Models\Material() @endphp

<div class="form-group">
    <label for="region_id">@lang('cp.region_id')</label>
    <select id="region_id" name="region_id" class="form-control select2">
        <option></option>
        @foreach($regions as $region)
            <option @if(old('region_id', $model->region_id) == $region->id) selected @endif value="{{ $region->id }}">{{ $region->name }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="inspector_id">@lang('cp.inspector')</label>
    <select id="inspector_id" name="inspector_id" class="form-control select2">
        <option></option>
        @foreach($inspectors as $inspector)
            <option @if(old('inspector_id', $model->inspector_id) == $inspector->id) selected @endif value="{{ $inspector->id }}">{{ $inspector->socrName() }} ({{ $inspector->region->name }})</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="reg_event_id">@lang('cp.reg_event_id')</label>
    <select id="reg_event_id" name="reg_event_id" class="form-control select2">
        <option></option>
        @foreach($regevents as $one)
            <option @if(old('reg_event_id', $model->reg_event_id) == $one->id) selected @endif value="{{ $one->id }}">{{ $one->name }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="date_receipt">@lang('cp.date_receipt')</label>
    <input type="text" id="date_receipt" name="date_receipt" placeholder="@lang('cp.date_receipt')" value="{{ old('date_receipt', $model->date_receipt) }}"
           class="form-control datepicker">
</div>

<div class="form-group">
    <label for="thematic_id">@lang('cp.thematic_id')</label>
    <select id="thematic_id" name="thematic_id" class="form-control select2">
        <option></option>
        @foreach(\App\Models\Thematic::all() as $thematic)
            <option @if(old('thematic_id', $model->thematic_id) == $thematic->id) selected @endif value="{{ $thematic->id }}">{{ $thematic->name }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="description">@lang('cp.description')</label>
    <textarea id="description" name="description" placeholder="@lang('cp.description')" class="form-control">{{ old('description', $model->description) }}</textarea>
</div>

<div class="form-group">
    <label for="count_warnings">@lang('cp.count_warnings')</label>
    <input type="text" id="count_warnings" name="count_warnings" placeholder="@lang('cp.count_warnings')" value="{{ old('count_warnings', $model->count_warnings) }}"
           class="form-control">
</div>

<hr>

<div class="form-group">
    <label for="confirmeted_warnings">@lang('cp.confirmeted_warnings')</label>
    <input type="text" id="confirmeted_warnings" name="confirmeted_warnings" placeholder="@lang('cp.confirmeted_warnings')" value="{{ old('confirmeted_warnings', $model->confirmeted_warnings) }}"
           class="form-control">
</div>

<div class="checkbox icheck">
    <label>
        <input type="checkbox" name="is_criminal"
               class="icheck" {{ old('is_criminal', $model->is_criminal) ? 'checked' : '' }} value="1">
        @lang('cp.is_criminal')
    </label>
</div>

<div class="form-group">
    <label for="count_protocols">@lang('cp.count_protocols')</label>
    <input type="text" id="count_protocols" name="count_protocols" placeholder="@lang('cp.count_protocols')" value="{{ old('count_protocols', $model->count_protocols) }}"
           class="form-control">
</div>

<div class="form-group">
    <label for="sum_penaltys">@lang('cp.sum_penaltys')</label>
    <input type="text" id="sum_penaltys" name="sum_penaltys" placeholder="@lang('cp.sum_penaltys')" value="{{ old('sum_penaltys', $model->sum_penaltys) }}"
           class="form-control">
</div>

<hr>

<div class="form-group">
    <label for="count_cancel_protocols">@lang('cp.count_cancel_protocols')</label>
    <input type="text" id="count_cancel_protocols" name="count_cancel_protocols" placeholder="@lang('cp.count_cancel_protocols')" value="{{ old('count_cancel_protocols', $model->count_cancel_protocols) }}"
           class="form-control">
</div>

<div class="form-group">
    <label for="notice">@lang('cp.notice')</label>
    <textarea id="notice" name="notice" placeholder="@lang('cp.notice')" class="form-control">{{ old('notice', $model->notice) }}</textarea>
</div>

<div class="form-group">
    <label for="images">@lang('cp.images')</label>
    <input type="file" id="images" name="images[]" multiple placeholder="@lang('cp.images')"
           class="form-control">

    @if ($model->id)
        <hr>
        <div class="upload-images">
            @include('cp.parts.images-lists')
        </div>
        @section('scripts')
            <script>
                $(function () {
                    initDeleteImages("{{ route('materials.delete_file', $model) }}");
                });
            </script>
        @endsection
    @endif
</div>