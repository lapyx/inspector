@extends('cp.layouts.default')

@section('page-title', __('cp.create').": ".__('cp.material'))

@section('page-header')

    <h1>@lang('cp.create')</h1>

@endsection

@section('content')

    <div class="panel panel-default">

        <div class="panel-body">

            @include('cp.parts.actions', ['controller'=>'materials'])

        </div>

    </div>

    <form action="{{ route('materials.store') }}" method="POST" enctype="multipart/form-data">

        {{ csrf_field() }}

        <div class="panel panel-default">

            <div class="panel-body">

                @include('cp.materials.form')

                <div class="form-group pull-right">
                    <button class="btn btn-primary">@lang('cp.create')</button>
                </div>

            </div>

        </div>

    </form>


@endsection
