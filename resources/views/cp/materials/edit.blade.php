@php /** @var \App\Models\Inspector $model */@endphp
@extends('cp.layouts.default')

@section('page-title', __('cp.edit').": ".__('cp.material')." $model->id")

@section('page-header')

    <h1>@lang('cp.edit')
        <small>ID: {{ $model->id }}</small>
    </h1>

@endsection

@section('content')

    <div class="panel panel-default">

        <div class="panel-body">

            @include('cp.parts.actions', ['controller'=>'materials', 'object'=>$model])

        </div>

    </div>

    <form action="{{ route('materials.update', $model) }}" method="POST" enctype="multipart/form-data">

        <input type="hidden" name="_method" value="PUT">

        {{ csrf_field() }}

        <div class="panel panel-default">

            <div class="panel-body">

                @include('cp.materials.form', ['model' => $model])

                <div class="form-group pull-right">
                    <button class="btn btn-primary">@lang('cp.save')</button>
                </div>

            </div>

        </div>

    </form>


@endsection
