@section('page-title', config('app.fullname'))

@extends('cp.layouts.default')

@section('content')

<link href="{{ asset('vendor/instruction/css/docs.min.css') }}" rel="stylesheet">

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">

            <div class="panel-body">
                <!-- Docs page layout -->
                <div class="bs-docs-header" id="content" tabindex="-1" style="padding:10px;">
                    <div class="container">
                        <h1>Инструкция</h1>
                        <p>По работе с системой</p>
                    </div>
                </div>

                <div class="container bs-docs-container">
                    <div class="row">
                        <div class="col-md-9" role="main">

                            <div class="bs-docs-section">

                                <h1 id="enter" class="page-header">Вход в сервис</h1>
                                <!-- <p class="lead"></p> -->
                                <p>
                                    Перейдите по адресу <a href="{{ route('cp') }}/login" target="_blank" rel="noopener">{{ route('cp') }}/login</a> и введите свой email и пароль, нажмите вход.
                                </p>

                                <p>
                                    На главной странице в левом боковом меню будет находится навигация сайта, которые вам доступно согласно ваших прав.
                                </p>
                                <div id="Screenshot_2" class="collapse">
                                    {{--<img src="{{ asset('vendor/instruction/img/Screenshot_2.png') }}" class="img-responsive" alt="">--}}
                                </div>

                            </div>


                        </div>
                        <div class="col-md-3">
                            <div class="bs-docs-sidebar hidden-print hidden-xs hidden-sm" data-spy="affix" data-offset-top="350" data-offset-bottom="100">
                                <ul class="nav bs-docs-sidenav">
                                    <li>
                                        <a href="#enter">Вход в сервис</a>
                                    </li>
                                </ul>
                                <a class="back-to-top" href="#top">Наверх</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script src="{{ asset('vendor/instruction/js/docs.min.js') }}"></script>
@endsection





