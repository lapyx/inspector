<?php

return [
    'groups' => [
        'rbac.permissions'   => 'Разрешения',
        'rbac.roles'         => 'Роли',
        'users'              => 'Пользователи',
        'inspectors'         => 'Инспекторы',
        'materials'         =>  'Реестр Материалов',
        'reg_events'         => 'Ресстр Мероприятий',
        'other' => [
            'name' => 'Другие',
            'lists' => []
        ]
    ],
];
