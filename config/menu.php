<?php

return [
//    Example: 'site' => [
//        'lang' => 'cp.site',
//        'img'  => 'fa-sitemap',
//        'lists' => [
//            ['pages',         'cp.site_menu'],
//            ['components',    'cp.site_components'],
//            ['questions',     'cp.site_questions'],
//            ['comments',      'cp.site_comments'],
//            ['htmltemplates', 'cp.htmltemplates'],
//            ['search',        'cp.site_search'],
//            ['news',          'cp.site_news_all'],
//            ['tenders',       'cp.site_tenders'],
//            ['shares',        'cp.site_shares'],
//        ]
//    ],
//    'filemanager' => [
//        'lang' => 'cp.file_manager',
//        'img'  => 'fa-file-image-o'
//    ],
    'inspectors' => [
        'lang' => 'cp.inspectors',
        'img'  => 'fa-address-book'
    ],
    'materials' => [
        'lang' => 'cp.materials',
        'img'  => 'fa-briefcase'
    ],
    'reg_events' => [
        'lang' => 'cp.reg_events',
        'img'  => 'fa-calendar'
    ],
    'rbac' => [
        'lang' => 'cp.rbac',
        'img'  => 'fa-lock',
        'lists' => [
            ['roles', 'cp.roles'],
            ['permissions', 'cp.permissions']
        ]
    ],
    'users' => [
        'lang' => 'cp.users',
        'img'  => 'fa-user'
    ]
];
