<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReadonlyInTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reg_events', function (Blueprint $table) {
            $table->boolean('readonly')->default(0)->comment('Запрет редактирования')->after('description');
        });

        Schema::table('inspectors', function (Blueprint $table) {
            $table->boolean('readonly')->default(0)->comment('Запрет редактирования')->after('description');
        });

        Schema::table('materials', function (Blueprint $table) {
            $table->boolean('readonly')->default(0)->comment('Запрет редактирования')->after('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reg_events', function (Blueprint $table) {
            $table->dropColumn('readonly');
        });

        Schema::table('inspectors', function (Blueprint $table) {
            $table->dropColumn('readonly');
        });

        Schema::table('materials', function (Blueprint $table) {
            $table->dropColumn('readonly');
        });
    }
}
