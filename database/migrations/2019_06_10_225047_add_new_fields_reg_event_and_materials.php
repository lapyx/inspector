<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsRegEventAndMaterials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reg_events', function (Blueprint $table) {
            $table->boolean('is_control')->default(0)->comment('Надзорное мероприятие?')->after('is_joint');
            $table->string('name')->nullable()->after('id');
        });

        Schema::create('confirmations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::table('materials', function (Blueprint $table) {
            $table->dropColumn('is_confirmed');
            $table->dropColumn('is_canceled');

            $table->unsignedInteger('reg_event_id')->nullable()->after('id');

            $table->integer('count_protocols')->default(0)->comment('Составлено протоколов')->after('count_warnings');
            $table->integer('count_cancel_protocols')->default(0)->comment('Отменено протоколов')->after('count_protocols');
            $table->unsignedInteger('confirmation_id')->nullable()->after('reg_event_id');

            $table->foreign('reg_event_id')->references('id')->on('reg_events')->onDelete('set null');
            $table->index('reg_event_id');

            $table->foreign('confirmation_id')->references('id')->on('confirmations')->onDelete('set null');
            $table->index('confirmation_id');
        });

        DB::table('confirmations')->insert([
            ['id' => 1, 'name' => 'Информация не подтвердилась'],
            ['id' => 2, 'name' => 'В полном объеме'],
            ['id' => 3, 'name' => 'Частично'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
