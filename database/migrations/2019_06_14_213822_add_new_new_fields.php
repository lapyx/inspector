<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewNewFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspectors', function (Blueprint $table) {
            $table->date('date_end_authority')->nullable()->comment('Дата прекращения полномочий')->after('doc_date_end');

            Schema::table('materials', function (Blueprint $table) {
                $table->integer('confirmeted_warnings')->nullable()->comment('Подтверждено нарушений')->after('count_warnings');

                $table->dropForeign('materials_confirmation_id_foreign');
                $table->dropIndex('materials_confirmation_id_index');
                $table->dropColumn('confirmation_id');
            });

            Schema::dropIfExists('confirmations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
