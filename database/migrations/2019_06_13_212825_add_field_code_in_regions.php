<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldCodeInRegions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('regions', function (Blueprint $table) {
            $table->string('code', 10)->nullable();
        });

        DB::statement('update regions set code=IF(id<10,concat("0",id),id)');

        DB::statement('update regions set code="95" where id = 20');
        DB::statement('update regions set code="83" where id = 80');
        DB::statement('update regions set code="86" where id = 81');
        DB::statement('update regions set code="87" where id = 82');
        DB::statement('update regions set code="89" where id = 83');
        DB::statement('update regions set code="82" where id = 84');
        DB::statement('update regions set code="92" where id = 85');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('regions', function (Blueprint $table) {
            $table->dropColumn('code');
        });
    }
}
