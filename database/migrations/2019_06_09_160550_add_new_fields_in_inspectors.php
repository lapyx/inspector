<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsInInspectors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspectors', function (Blueprint $table) {
            $table->string('phone', 20)->nullable()->after('id');
            $table->string('email', 20)->nullable()->after('phone');
            $table->string('photo', 500)->nullable()->after('region_id');
            $table->string('images', 5000)->default('[]')->after('region_id');

            $table->date('date_statement2')->nullable()->comment('Дата 2 подачи заявления')->after('date_statement');
            $table->date('date_statement3')->nullable()->comment('Дата 3 подачи заявления')->after('date_statement');

            $table->date('date_renewal1')->nullable()->comment('Дата 1 продления')->after('date_statement3');
            $table->date('date_renewal2')->nullable()->comment('Дата 2 продления')->after('date_statement3');
            $table->date('date_renewal3')->nullable()->comment('Дата 3 продления')->after('date_statement3');

            $table->string('job')->nullable()->comment('Место работы')->after('date_renewal3');
            $table->string('public_org', 2000)->nullable()->comment('Членство в общественных организациях')->after('job');

            $table->unsignedInteger('user_id')->nullable()->after('description');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->index('user_id');
        });

        DB::statement("ALTER TABLE `inspectors`
	      CHANGE COLUMN `date_statement` `date_statement1` DATE NULL DEFAULT NULL COMMENT 'Дата 1 подачи заявления' AFTER `region_id`");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
