<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertRegions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Республика Адыгея')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Республика Башкортостан')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Республика Бурятия')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Республика Алтай')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Республика Дагестан')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Республика Ингушетия')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Кабардино-Балкарская Республика')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Республика Калмыкия')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Карачаево-Черкесская Республика')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Республика Карелия')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Республика Коми')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Республика Марий Эл')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Республика Мордовия')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Республика Саха (Якутия)')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Республика Северная Осетия - Алания')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Республика Татарстан')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Республика Тыва')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Удмуртская Республика')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Республика Хакасия')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Чеченская Республика')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Чувашская Республика')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Алтайский край')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Краснодарский край')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Красноярский край')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Приморский край')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Ставропольский край')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Хабаровский край')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Амурская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Архангельская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Астраханская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Белгородская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Брянская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Владимирская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Волгоградская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Вологодская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Воронежская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Ивановская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Иркутская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Калининградская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Калужская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Камчатский край')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Кемеровская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Кировская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Костромская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Курганская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Курская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Ленинградская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Липецкая область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Магаданская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Московская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Мурманская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Нижегородская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Новгородская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Новосибирская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Омская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Оренбургская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Орловская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Пензенская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Пермский край')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Псковская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Ростовская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Рязанская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Самарская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Саратовская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Сахалинская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Свердловская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Смоленская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Тамбовская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Тверская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Томская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Тульская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Тюменская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Ульяновская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Челябинская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Забайкальский край')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Ярославская область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Москва')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Санкт-Петербург')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Еврейская автономная область')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Ненецкий автономный округ')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Ханты-Мансийский автономный округ')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Чукотский автономный округ')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Ямало-Ненецкий автономный округ')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Республика Крым')");
       DB::statement("INSERT INTO `regions` (`name`) VALUES ('Севастополь')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('regions')->delete();
    }
}
