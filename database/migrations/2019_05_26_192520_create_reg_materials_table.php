<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('thematics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('materials', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('inspector_id');
            $table->unsignedInteger('thematic_id')->nullable();
            $table->string('images', 5000)->default('[]');
            $table->date('date_receipt')->nullable()->comment('Дата поступления материалов');
            $table->boolean('is_criminal')->default(0)->comment('Возбуждено дело на основании поступивших материалов ?');
            $table->boolean('is_confirmed')->default(0)->comment('Информация подтвердилась ?');
            $table->boolean('is_canceled')->default(0)->comment('Протоколы отменены ?');
            $table->integer('count_warnings')->default(0)->comment('Выявлено нарушений');
            $table->decimal('sum_penaltys', 10, 2)->default(0)->comment('Наложено штрафов на сумму, тыс. руб.');
            $table->string('description', 3000)->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('inspector_id')->references('id')->on('inspectors');
            $table->index('inspector_id');

            $table->foreign('thematic_id')->references('id')->on('thematics');
            $table->index('thematic_id');
        });

        DB::table('rbac_permissions')->insert([
            ['id' => 17, 'name' => 'materials.show',    'display_name' => 'Просмотр реестр материалов'],
            ['id' => 18, 'name' => 'materials.edit',    'display_name' => 'Редактирование реестр материалов'],
            ['id' => 19, 'name' => 'materials.create',  'display_name' => 'Добавление реестр материалов'],
            ['id' => 20, 'name' => 'materials.destroy', 'display_name' => 'Удаление реестр материалов'],
        ]);

        DB::table('rbac_permission_role')->insert([
            ['permission_id' => 17, 'role_id' => \App\Enum\RoleType::ADMIN],
            ['permission_id' => 18, 'role_id' => \App\Enum\RoleType::ADMIN],
            ['permission_id' => 19, 'role_id' => \App\Enum\RoleType::ADMIN],
            ['permission_id' => 20, 'role_id' => \App\Enum\RoleType::ADMIN],
        ]);

        Artisan::call('cache:clear');
        Artisan::call('config:clear');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materials');
        Schema::dropIfExists('thematics');
    }
}
