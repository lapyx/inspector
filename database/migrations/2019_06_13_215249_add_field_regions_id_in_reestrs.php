<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldRegionsIdInReestrs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('materials', function (Blueprint $table) {
            $table->unsignedInteger('region_id')->nullable()->after('thematic_id');

            $table->foreign('region_id')->references('id')->on('regions');
            $table->index('region_id');
        });

        Schema::table('reg_events', function (Blueprint $table) {
            $table->unsignedInteger('region_id')->nullable()->after('inspector_id');

            $table->foreign('region_id')->references('id')->on('regions');
            $table->index('region_id');
        });

        foreach (\App\Models\Material::withTrashed()->get() as $one){
            $one->region_id = $one->inspector->region_id;
            $one->save();
        }

        foreach (\App\Models\RegEvent::withTrashed()->get() as $one){
            $one->region_id = $one->inspector->region_id;
            $one->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('materials', function (Blueprint $table) {
            $table->dropForeign('materials_region_id_foreign');
            $table->dropIndex('materials_region_id_index');
            $table->dropColumn('region_id');
        });

        Schema::table('reg_events', function (Blueprint $table) {
            $table->dropForeign('reg_events_region_id_foreign');
            $table->dropIndex('reg_events_region_id_index');
            $table->dropColumn('region_id');
        });
    }
}
