<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeasuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reg_events', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('inspector_id');
            $table->string('images', 5000)->default('[]');
            $table->date('date_event')->nullable()->comment('Дата проведенного ОИ мероприятия по контролю');
            $table->boolean('is_joint')->default(0)->comment('Совместное ?');
            $table->integer('count_warnings')->default(0)->comment('Выявлено нарушений');
            $table->decimal('sum_penaltys', 10, 2)->default(0)->comment('Наложено штрафов на сумму, тыс. руб.');
            $table->string('description', 3000)->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('inspector_id')->references('id')->on('inspectors');
            $table->index('inspector_id');
        });

        DB::table('rbac_permissions')->insert([
            ['id' => 21, 'name' => 'reg_events.show',    'display_name' => 'Просмотр реестр мероприятий'],
            ['id' => 22, 'name' => 'reg_events.edit',    'display_name' => 'Редактирование реестр мероприятий'],
            ['id' => 23, 'name' => 'reg_events.create',  'display_name' => 'Добавление реестр мероприятий'],
            ['id' => 24, 'name' => 'reg_events.destroy', 'display_name' => 'Удаление реестр мероприятий'],
        ]);

        DB::table('rbac_permission_role')->insert([
            ['permission_id' => 21, 'role_id' => \App\Enum\RoleType::ADMIN],
            ['permission_id' => 22, 'role_id' => \App\Enum\RoleType::ADMIN],
            ['permission_id' => 23, 'role_id' => \App\Enum\RoleType::ADMIN],
            ['permission_id' => 24, 'role_id' => \App\Enum\RoleType::ADMIN],
        ]);

        Artisan::call('cache:clear');
        Artisan::call('config:clear');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measures');
    }
}
