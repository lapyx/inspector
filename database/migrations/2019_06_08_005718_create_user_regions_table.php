<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_regions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('region_id');

            $table->foreign('region_id')->references('id')->on('regions')->onDelete('cascade');
            $table->index('region_id');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->index('user_id');

            $table->unique(['user_id', 'region_id']);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_region_id_foreign');
            $table->dropIndex('users_region_id_index');
            $table->dropColumn('region_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_regions');

        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('region_id')->nullable()->after('email');

            $table->foreign('region_id')->references('id')->on('regions');
            $table->index('region_id');
        });
    }
}
