<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class DataPermissionAndRolesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        DB::table('rbac_permissions')->insert([
            ['id' => 1,  'name' => 'rbac.permissions.create', 'display_name' => 'Добавление разрешений'],
            ['id' => 2,  'name' => 'rbac.permissions.edit', 'display_name' => 'Редактирование разрешений'],
            ['id' => 3,  'name' => 'rbac.permissions.show', 'display_name' => 'Просмотр разрешений'],
            ['id' => 4,  'name' => 'rbac.permissions.destroy', 'display_name' => 'Удаление разрешений'],
            ['id' => 5, 'name' => 'rbac.roles.create', 'display_name' => 'Добавление ролей'],
            ['id' => 6, 'name' => 'rbac.roles.edit', 'display_name' => 'Редактирование ролей'],
            ['id' => 7, 'name' => 'rbac.roles.show', 'display_name' => 'Просмотр ролей'],
            ['id' => 8, 'name' => 'rbac.roles.destroy', 'display_name' => 'Удаление ролей'],
            ['id' => 9, 'name' => 'users.destroy', 'display_name' => 'Удаление пользователей'],
            ['id' => 10, 'name' => 'users.show', 'display_name' => 'Просмотр пользователей'],
            ['id' => 11, 'name' => 'users.edit', 'display_name' => 'Редактирование пользователей'],
            ['id' => 12, 'name' => 'users.create', 'display_name' => 'Добавление пользователей'],
        ]);

        DB::table('rbac_roles')->insert([
            ['id' => 1, 'name' => 'admin', 'display_name' => 'Админ'],
            ['id' => 2, 'name' => 'guest', 'display_name' => 'Гость'],
        ]);

        DB::table('rbac_permission_role')->insert([
            ['permission_id' => 1, 'role_id' => 1],
            ['permission_id' => 2, 'role_id' => 1],
            ['permission_id' => 3, 'role_id' => 1],
            ['permission_id' => 4, 'role_id' => 1],
            ['permission_id' => 5, 'role_id' => 1],
            ['permission_id' => 6, 'role_id' => 1],
            ['permission_id' => 7, 'role_id' => 1],
            ['permission_id' => 8, 'role_id' => 1],
            ['permission_id' => 9, 'role_id' => 1],
            ['permission_id' => 10, 'role_id' => 1],
            ['permission_id' => 11, 'role_id' => 1],
            ['permission_id' => 12, 'role_id' => 1],
        ]);

        $user = \App\Models\User::find(1);
        $user->attachRole('admin');
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        DB::table('rbac_permissions')->truncate();
        DB::table('rbac_roles')->truncate();
        DB::table('rbac_permission_role')->truncate();
    }
}
