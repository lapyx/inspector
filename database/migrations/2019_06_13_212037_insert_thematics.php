<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertThematics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('materials')->update(['thematic_id' => null]);
        DB::table('thematics')->delete();

        DB::table('thematics')->insert([
            ['id' => 1, 'name' => 'Охрана атмосферного воздуха'],
            ['id' => 2, 'name' => 'Охрана водных объектов'],
            ['id' => 3, 'name' => 'Обращение с отходами'],
            ['id' => 4, 'name' => 'Охрана почв'],
            ['id' => 5, 'name' => 'Охрана недр'],
            ['id' => 6, 'name' => 'Охрана ООПТ'],
            ['id' => 7, 'name' => 'Обращение с краснокнижными животными/растениями'],
            ['id' => 8, 'name' => 'Обращение с некраснокнижными животными'],
            ['id' => 9, 'name' => 'Иное'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('thematics')->truncate();
    }
}
