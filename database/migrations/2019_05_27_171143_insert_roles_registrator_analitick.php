<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Enum\RoleType;

class InsertRolesRegistratorAnalitick extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Models\RBAC\Role::insert([
           ['id' => RoleType::REGISTRAR, 'name' => 'registrar', 'display_name' => 'Регистратор'],
           ['id' => RoleType::ANALYST,   'name' => 'analyst', 'display_name' => 'Аналитик'],
        ]);

        DB::table('rbac_permission_role')->insert([
            ['permission_id' => 13, 'role_id' => RoleType::REGISTRAR],
            ['permission_id' => 14, 'role_id' => RoleType::REGISTRAR],
            ['permission_id' => 15, 'role_id' => RoleType::REGISTRAR],
            ['permission_id' => 16, 'role_id' => RoleType::REGISTRAR],
            ['permission_id' => 17, 'role_id' => RoleType::REGISTRAR],
            ['permission_id' => 18, 'role_id' => RoleType::REGISTRAR],
            ['permission_id' => 19, 'role_id' => RoleType::REGISTRAR],
            ['permission_id' => 20, 'role_id' => RoleType::REGISTRAR],
            ['permission_id' => 21, 'role_id' => RoleType::REGISTRAR],
            ['permission_id' => 22, 'role_id' => RoleType::REGISTRAR],
            ['permission_id' => 23, 'role_id' => RoleType::REGISTRAR],
            ['permission_id' => 24, 'role_id' => RoleType::REGISTRAR],

            ['permission_id' => 13, 'role_id' => RoleType::ANALYST],
            ['permission_id' => 17, 'role_id' => RoleType::ANALYST],
            ['permission_id' => 21, 'role_id' => RoleType::ANALYST],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('rbac_permission_role')->whereIn('role_id', [RoleType::REGISTRAR, RoleType::ANALYST])->delete();
        DB::table('rbac_roles')->whereIn('id', [RoleType::REGISTRAR, RoleType::ANALYST])->delete();
    }
}
