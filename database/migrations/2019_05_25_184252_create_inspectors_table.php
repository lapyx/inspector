<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInspectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('inspectors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fam');
            $table->string('nam');
            $table->string('otch')->nullable();
            $table->date('birthday');
            $table->enum('sex', ['M', 'F'])->nullable();
            $table->unsignedInteger('region_id');
            $table->date('date_statement')->nullable()->comment('Дата подачи заявления');
            $table->date('doc_date_begin')->nullable()->comment('Дата выдачи удостоверения');
            $table->date('doc_date_end')->nullable()->comment('Дата сдачи удостоверения');
            $table->string('doc_number')->nullable()->comment('Номер удостоверения');
            $table->string('description', 3000)->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('region_id')->references('id')->on('regions');
            $table->index('region_id');
        });

        DB::table('rbac_permissions')->insert([
            ['id' => 13, 'name' => 'inspectors.show',    'display_name' => 'Просмотр Инспекторов'],
            ['id' => 14, 'name' => 'inspectors.edit',    'display_name' => 'Редактирование Инспекторов'],
            ['id' => 15, 'name' => 'inspectors.create',  'display_name' => 'Добавление Инспекторов'],
            ['id' => 16, 'name' => 'inspectors.destroy', 'display_name' => 'Удаление Инспекторов'],
        ]);

        DB::table('rbac_permission_role')->insert([
            ['permission_id' => 13, 'role_id' => \App\Enum\RoleType::ADMIN],
            ['permission_id' => 14, 'role_id' => \App\Enum\RoleType::ADMIN],
            ['permission_id' => 15, 'role_id' => \App\Enum\RoleType::ADMIN],
            ['permission_id' => 16, 'role_id' => \App\Enum\RoleType::ADMIN],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inspectors');
        Schema::dropIfExists('regions');
    }
}
