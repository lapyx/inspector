<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsInReestr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspectors', function (Blueprint $table) {
            $table->string('placelive', 500)->nullable()->comment('Место жительства')->after('birthday');

            DB::statement("ALTER TABLE `inspectors`
	          CHANGE COLUMN `description` `notice` TEXT NULL DEFAULT NULL COMMENT 'Примечание' AFTER `region_id`");

            Schema::table('reg_events', function (Blueprint $table) {
                $table->text('notice')->nullable()->comment('Примечание')->after('description');
                $table->text('description')->nullable()->change();
            });

            Schema::table('materials', function (Blueprint $table) {
                $table->text('notice')->nullable()->comment('Примечание')->after('description');
                $table->text('description')->nullable()->change();
            });
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inspectors', function (Blueprint $table) {
            $table->dropColumn('placelive');
        });

        DB::statement("ALTER TABLE `inspectors`
	      CHANGE COLUMN `notice` `description` TEXT NULL DEFAULT NULL COMMENT 'Примечание' AFTER `region_id`");

        Schema::table('reg_events', function (Blueprint $table) {
            $table->dropColumn('notice');
        });

        Schema::table('materials', function (Blueprint $table) {
            $table->dropColumn('notice');
        });
    }
}
