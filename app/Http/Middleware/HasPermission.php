<?php

namespace App\Http\Middleware;

use Laratrust\Middleware\LaratrustPermission;
use Illuminate\Support\Facades\Config;
use Illuminate\Contracts\Auth\Guard;
use Closure;
use Illuminate\Support\Facades\Route;
//use Illuminate\Support\Facades\Auth;

class HasPermission
{
    const DELIMITER = '|';

    protected $auth;

    /**
     * Creates a new instance of the middleware.
     *
     * @param  Guard  $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $as     = array_get($request->route()->getAction(), 'as');
        $prefix = array_get($request->route()->getAction(), 'prefix');

        $permissions = $as;

        if ($prefix) {
            $prefix = str_replace('/', '.', $prefix);
            $firstCharAs = substr($as, 0, strlen($prefix));
            $lastCharAs = substr($as, strlen($prefix));

            if ($firstCharAs == $prefix) {
                $permissions = ltrim($lastCharAs, '.');
            }
        }

        if (!is_array($permissions)) {
            $permissions = explode(self::DELIMITER, $permissions);
        }

        if (!$as || $this->auth->guest() || !$request->user()->hasPermission($permissions)) {
            return abort('404');
        }

        return $next($request);
    }
}
