<?php

namespace App\Http\Middleware;

/**
 * This file is part of Laratrust,
 * a role & permission management solution for Laravel.
 *
 * @license MIT
 * @package Laratrust
 */

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Config;
use Laratrust\Middleware\LaratrustPermission as BaseLaratrustPermission;

use Log;

class LaratrustPermission extends BaseLaratrustPermission
{
    const DELIMITER = '|';

    protected $auth;

    /**
     * Creates a new instance of the middleware.
     *
     * @param  Guard  $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Closure  $next
     * @param  $permissions
     * @return mixed
     */
    public function handle($request, Closure $next, $permissions, $team = null, $requireAll = false)
    {
        list($team, $requireAll) = $this->assignRealValuesTo($team, $requireAll);

        if (!is_array($permissions)) {
            $permissions = explode(self::DELIMITER, $permissions);
        }

        if ($this->auth->guest() || !$request->user()->hasPermission($permissions, $team, $requireAll)) {

            // Log::debug(' Denied user '.$request->user()->id.' permit:'.print_r($permissions, 1).', team'.print_r($team, 1 ).' , requireAll'.print_r($requireAll, 1 ) );
            Log::error('Не достаточно прав:'.print_r($permissions,1));
            if (request()->ajax()) {
                return response()->json(['error' => 'У вас не достаточно прав ('.implode(',', $permissions).')']);
            } else {
                return redirect()->route('cp')->with('errors', ['У вас не достаточно прав ('.implode(',', $permissions).')']);
            }
        }

        return $next($request);
    }

    /**
     * Assing the real values to the team and requireAllOrOptions parameters.
     *
     * @param  mixed  $team
     * @param  mixed  $requireAllOrOptions
     * @return array
     */
    private function assignRealValuesTo($team, $requireAllOrOptions)
    {
        return [
            ($team == 'require_all' ? null : $team),
            ($team == 'require_all' ? true : ($requireAllOrOptions== 'require_all' ? true : false)),
        ];
    }
}
