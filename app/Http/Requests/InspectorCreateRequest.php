<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InspectorCreateRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nam'             => 'required|max:255',
            'fam'             => 'required|max:255',
            'otch'            => 'max:255',
            'email'           => 'nullable|email',
            'phone'           => 'nullable|phone',
            'birthday'        => 'required|date|date_format:d.m.Y',
            'sex'             => 'nullable|in:M,F',
            'region_id'       => 'required|exists:regions,id',
            'date_statement1' => 'nullable|date',
            'date_end_authority' => 'nullable|date',
            'date_statement2' => 'nullable|date|after:date_statement1',
            'date_statement3' => 'nullable|date|after:date_statement2',
            'date_renewal1'   => 'nullable|date',
            'date_renewal2'   => 'nullable|date|after:date_renewal1',
            'date_renewal3'   => 'nullable|date|after:date_renewal2',
            'doc_date_begin'  => 'nullable|date',
            'date_statement_doc'  => 'nullable|date',
            'doc_date_end'    => 'nullable|date|after:doc_date_begin',
            'doc_number'      => 'max:255',
            'job'             => 'max:255',
            'placelive'       => 'max:255',
            'public_org'      => 'max:2000',
            'notice'          => 'max:3000',
            'images.*'        => 'nullable|mimes:jpeg,jpg,png,gif,pdf,zip,rar|max:10000',
            'photo'           => 'nullable|mimes:jpeg,jpg,png,gif|max:5000'
        ];
    }
}
