<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegEventCreateRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $model = $this->route('reg_event');
        return [
            'name'           => 'required|max:255|unique:reg_events,name,'.($model ? $model->id : 0),
            'inspector_id'   => 'required|exists:inspectors,id',
            'region_id'      => 'required|exists:regions,id',
            'date_event'     => 'nullable|date',
            'is_joint'       => 'nullable|boolean',
            'is_control'     => 'nullable|boolean',
            'count_warnings' => 'nullable|integer',
            'sum_penaltys'   => 'nullable|numeric',
            'description'    => 'max:5000',
            'notice'         => 'max:5000',
            'images.*'       => 'nullable|mimes:jpeg,jpg,png,gif,pdf,zip,rar|max:10000'
        ];
    }
}
