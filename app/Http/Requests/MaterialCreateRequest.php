<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MaterialCreateRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'inspector_id'    => 'required|exists:inspectors,id',
            'reg_event_id'    => 'nullable|exists:reg_events,id',
            'thematic_id'     => 'nullable',
            'region_id'       => 'required|exists:regions,id',
            'date_receipt'    => 'nullable|date',
            'is_criminal'     => 'nullable|boolean',
            'count_warnings'  => 'nullable|integer',
            'count_protocols' => 'nullable|integer',
            'confirmeted_warnings' => 'nullable|integer',
            'count_cancel_protocols' => 'nullable|integer',
            'sum_penaltys'   => 'nullable|numeric',
            'description'    => 'max:5000',
            'notice'         => 'max:5000',
            'images.*'       => 'nullable|mimes:jpeg,jpg,png,gif,pdf,zip,rar|max:10000'
        ];
    }
}
