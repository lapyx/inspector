<?php

namespace App\Http\Controllers;

use App\Exceptions\AdminException;
use App\Http\Requests\InspectorCreateRequest;
use App\Models\Inspector;
use App\Repository\InspectorRepository;
use DataTables;
use Exception, DB;
use Spatie\Activitylog\Models\Activity;

class InspectorController extends Controller
{
    protected $table = 'inspectors';
    protected $inspectors;

    public function __construct(InspectorRepository $inspectors)
    {
        parent::__construct();

        $this->inspectors = $inspectors;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cp.parts.index', $this->datatableParams());
    }

    public function datatableParams()
    {
        return [
            'table' => $this->table,
            'url' => route("{$this->table}.datatables.data"),
            'columns' => [
                [ 'data' => 'id', 'name' => 'id' ],
                [ 'data' => 'fam', 'name' => 'fam' ],
                [ 'data' => 'nam', 'name' => 'nam' ],
                [ 'data' => 'otch', 'name' => 'otch' ],
                [ 'data' => 'birthday', 'name' => 'birthday' ],
                [ 'data' => 'region_name', 'name' => 'region_name' ],
                [ 'data' => 'doc_number', 'name' => 'doc_number' ],
                [ 'data' => 'registrator', 'name' => 'registrator' ],
                [ 'data' => 'action', 'name' => 'action', 'orderable'=> false, 'searchable'=> false]
            ]
        ];
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function anyData()
    {
        $query = $this->inspectors->queryByUser($this->user, ['inspectors.*', 'regions.name as region_name']);

        return Datatables::of($query)
            ->filterColumn('region_name', function($query, $keyword) {
                $query->whereRaw("regions.name like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('registrator', function($query, $keyword) {
                $query->whereRaw("CONCAT_WS(' ', nam, fam, otch) like ?", ["%{$keyword}%"]);
            })
            ->addColumn('id', function (Inspector $model) {
                return '<a href="'.route("{$this->table}.show", $model).'" class="btn btn-info btn-xs" title="Просмотр">'.$model->id.'</a>';
            })
            ->addColumn('registrator', function (Inspector $model) {
                return $model->user_id ? $model->user->socrName() : '';
            })
            ->addColumn('action', function (Inspector $model) {
                return view('cp.parts.actions', ['controller'=>$this->table, 'object' => $model, 'action'=>'table']);
            })
            ->editColumn('birthday', function (Inspector $model) {
                return $model->birthday;
            })
            ->rawColumns(['id', 'action'])
            ->make(true);
    }


    /**
     * @param Inspector $inspector
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show(Inspector $inspector)
    {
        return view("cp.{$this->table}.show", ['model' => $inspector]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        return $this->edit();
    }


    /**
     * @param InspectorCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(InspectorCreateRequest $request)
    {
        try {
            $data = $request->only(array_keys($request->rules()));

            $inspector = new Inspector();
            $data['images'] = $this->uploadFiles($inspector, 'images');
            $data['photo']  = $inspector->uploadPhoto();

            $inspector->fill($data);
            $inspector->save();

            return $this->redirectShow($inspector)->with('success', __('messages.create'));
        } catch (AdminException $e) {
            return $this->redirectBackErrorWith([$e->getMessage()]);
        }
    }

    /**
     * @param Inspector $inspector
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit(Inspector $inspector = null)
    {
        $params = ['regions' => $this->common->getRegionsByUser($this->user)];

        if ($inspector) {
            if (!$inspector->allowed($this->user, 'change'))
                return $this->redirectBackErrorWith('У вас недостаточно прав');

            return view("cp.{$this->table}.edit", ['model' => $inspector] + $params);
        } else {
            return view("cp.{$this->table}.create", $params);
        }
    }

    /**
     * @param InspectorCreateRequest $request
     * @param Inspector $inspector
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(InspectorCreateRequest $request, Inspector $inspector)
    {
        try {
            if (!$inspector->allowed($this->user, 'change'))
                throw new AdminException('У вас недостаточно прав');

            $data = $request->only(array_keys($request->rules()));
            $data['images'] = $this->uploadFiles($inspector, 'images');
            $data['photo']  = $inspector->uploadPhoto();

            $inspector->update($data);

            return $this->redirectShow($inspector)->with('success', __('messages.update'));
        } catch (AdminException $e) {
            return $this->redirectBackErrorWith([$e->getMessage()]);
        }
    }

    /**
     * @param Inspector $inspector
     * @return \Illuminate\Http\RedirectResponse
     * @throws Exception
     */
    public function destroy(Inspector $inspector)
    {
        try {
            if (!$inspector->allowed($this->user, 'delete'))
                throw new AdminException('У вас недостаточно прав');

            $inspector->delete();

            return $this->redirectIndex()->with('success', __('messages.delete'));
        } catch (AdminException $e) {
            return $this->redirectBackErrorWith([$e->getMessage()]);
        }
    }

    /**
     * @param Inspector $inspector
     * @return \Illuminate\Http\RedirectResponse
     */
    public function readonly(Inspector $inspector)
    {
        return $this->setReadonly($inspector);
    }

    /**
     * @param Inspector $inspector
     * @return array
     */
    public function deleteFile(Inspector $inspector)
    {
        $file = basename(request('file'));

        return $this->removeFiles($inspector, [$file]);
    }

    /**
     * выгрузка в Excel
     */
    public function getExcel()
    {
        $columns = \Schema::getColumnListing($this->table);
        // удаляем лишние колонки
        $columns = array_unset_values($columns, ['images', 'photo']);
        // меняем имена колонок
        $columns = array_swap_values($columns, ['user_id' => 'registrator']);

        $query = $this->inspectors->queryByUser($this->user, [
            'inspectors.*',
            'regions.name as region_id',
            'users.username as registrator',
            DB::raw("IF(readonly, 'Да', 'Нет') as readonly")
        ]);

        if ($this->user->hasRole('admin')) {
            $query = $query->withTrashed();
        }

        return $this->excelGenerate($columns, $query);
    }

    /**
     * @param Inspector $inspector
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function audit(Inspector $inspector)
    {
        return $this->getAudit($inspector);
    }

}
