<?php

namespace App\Http\Controllers;

class CPController extends Controller
{
    /**
     * Show the application control panel.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cp.index');
    }

    public function filemanager()
    {
        return view('cp.filemanager');
    }
    
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function instruction()
    {
        return view('cp.instruction');
    }

}
