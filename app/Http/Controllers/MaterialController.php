<?php

namespace App\Http\Controllers;

use App\Exceptions\AdminException;
use App\Http\Requests\MaterialCreateRequest;
use App\Models\Material;
use App\Repository\InspectorRepository;
use App\Repository\MaterialRepository;
use App\Repository\RegEventRepository;
use DataTables;
use Exception, DB;

/**
 * Class MaterialController
 * @package App\Http\Controllers
 */
class MaterialController extends Controller
{
    protected $table = 'materials';
    protected $regevents;
    protected $inspectors;
    protected $materials;

    /**
     * MaterialController constructor.
     * @param InspectorRepository $inspectors
     * @param RegEventRepository $regevents
     * @param MaterialRepository $materials
     */
    public function __construct(InspectorRepository $inspectors, RegEventRepository $regevents, MaterialRepository $materials)
    {
        parent::__construct();

        $this->inspectors = $inspectors;
        $this->regevents  = $regevents;
        $this->materials  = $materials;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cp.parts.index', $this->datatableParams());
    }

    public function datatableParams()
    {
        return [
            'table' => $this->table,
            'url' => route("{$this->table}.datatables.data"),
            'columns' => [
                [ 'data' => 'id', 'name' => 'id' ],
                [ 'data' => 'inspector', 'name' => 'inspector' ],
                [ 'data' => 'reg_event_id', 'name' => 'reg_event_id' ],
                [ 'data' => 'thematic_name', 'name' => 'thematic_name' ],
                [ 'data' => 'date_receipt', 'name' => 'date_receipt' ],
                [ 'data' => 'region_name', 'name' => 'region_name' ],
                [ 'data' => 'count_warnings', 'name' => 'count_warnings' ],
                [ 'data' => 'sum_penaltys', 'name' => 'sum_penaltys' ],
                [ 'data' => 'action', 'name' => 'action', 'orderable'=> false, 'searchable'=> false]
            ]
        ];
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function anyData()
    {
        $query = $this->materials->queryByUser($this->user, [
            'materials.*',
            'regions.name as region_name',
            'thematics.name as thematic_name',
            'reg_events.name as reg_event_name',
        ]);


        return DataTables::of($query)
            ->filterColumn('region_name', function($query, $keyword) {
                $query->whereRaw("regions.name like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('thematic_name', function($query, $keyword) {
                $query->whereRaw("thematics.name like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('reg_event_id', function($query, $keyword) {
                $query->whereRaw("reg_events.name like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('inspector', function($query, $keyword) {
                $query->whereRaw("CONCAT_WS(' ', nam, fam, otch) like ?", ["%{$keyword}%"]);
            })
            ->addColumn('id', function (Material $model) {
                return '<a href="'.route("{$this->table}.show", $model).'" class="btn btn-info btn-xs" title="Просмотр">'.$model->id.'</a>';
            })
            ->addColumn('action', function (Material $model) {
                return view('cp.parts.actions', ['controller'=>$this->table, 'object' => $model, 'action'=>'table']);
            })
            ->addColumn('inspector', function (Material $model) {
                return $model->inspector->getAhref();
            })
            ->addColumn('reg_event_id', function (Material $model) {
                return $model->reg_event_id ? $model->regEvent->getAhref() : '';
            })
            ->editColumn('date_receipt', function (Material $model) {
                return $model->date_receipt;
            })
            ->rawColumns(['id', 'reg_event_id', 'action', 'inspector'])
            ->make(true);
    }


    /**
     * @param Material $material
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show(Material $material)
    {
        return view("cp.{$this->table}.show", ['model' => $material]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        return $this->edit();
    }

    /**
     * @param MaterialCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(MaterialCreateRequest $request)
    {
        try {
            $data = $request->only(array_keys($request->rules()));

            $material = new Material();

            $data['images'] = $this->uploadFiles($material, 'images');

            $material = $material->newInstance();
            $material->fill($data);
            $material->save();

            return $this->redirectShow($material)->with('success', __('messages.create'));
        } catch (AdminException $e) {
            return $this->redirectBackErrorWith([$e->getMessage()]);
        }
    }

    /**
     * @param Material $material
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit(Material $material = null)
    {
        $params = [];
        $params['inspectors'] = $this->inspectors->getByRegion($this->user->getRegionsId());
        $params['regevents']  = $this->regevents->getByRegion($this->user->getRegionsId());
        $params['regions']    = $this->common->getRegionsByUser($this->user);

        if ($material) {
            if (!$material->allowed($this->user, 'change'))
                return $this->redirectBackErrorWith('У вас недостаточно прав');

            return view("cp.{$this->table}.edit", ['model' => $material] + $params);
        } else {
            return view("cp.{$this->table}.create", $params);
        }
    }

    /**
     * @param MaterialCreateRequest $request
     * @param Material $material
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(MaterialCreateRequest $request, Material $material)
    {
        try {
            if (! $material->allowed($this->user, 'change'))
                throw new AdminException('У вас недостаточно прав');

            $data = $request->only(array_keys($request->rules()));

            $data['images'] = $this->uploadFiles($material, 'images');

            $material->update($data);

            return $this->redirectShow($material)->with('success', __('messages.update'));
        } catch (AdminException $e) {
            return $this->redirectBackErrorWith([$e->getMessage()]);
        }
    }

    /**
     * @param Material $material
     * @return \Illuminate\Http\RedirectResponse
     * @throws Exception
     */
    public function destroy(Material $material)
    {
        try {
            if (! $material->allowed($this->user, 'delete'))
                throw new AdminException('У вас недостаточно прав');

            $material->delete();

            return $this->redirectIndex()->with('success', __('messages.delete'));
        } catch (AdminException $e) {
            return $this->redirectBackErrorWith([$e->getMessage()]);
        }
    }

    /**
     * @param Material $material
     * @return array
     */
    public function deleteFile(Material $material)
    {
        $file = basename(request('file'));

        return $this->removeFiles($material, [$file]);
    }

    /**
     * @param Material $material
     * @return \Illuminate\Http\RedirectResponse
     */
    public function readonly(Material $material)
    {
        return $this->setReadonly($material);
    }

    /**
     * выгрузка в Excel
     */
    public function getExcel()
    {
        $columns = \Schema::getColumnListing($this->table);
        // удаляем лишние колонки
        $columns = array_unset_values($columns, ['images']);

        $query = $this->materials->queryByUser($this->user, [
            'materials.*',
            DB::raw("CONCAT_WS(' ', nam, fam, otch) as inspector_id"),
            'regions.name as region_id',
            'thematics.name as thematic_id',
            'reg_events.name as reg_event_id',
            DB::raw("IF(materials.readonly, 'Да', 'Нет') as readonly"),
            DB::raw("IF(is_criminal, 'Да', 'Нет') as is_criminal")
        ]);

        if ($this->user->hasRole('admin')) {
            $query = $query->withTrashed();
        }

        return $this->excelGenerate($columns, $query);
    }

    /**
     * @param Material $material
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function audit(Material $material)
    {
        return $this->getAudit($material);
    }

}
