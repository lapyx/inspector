<?php

namespace App\Http\Controllers;

use App\Models\Inspector;
use App\Models\Material;
use App\Models\RegEvent;
use App\Models\User;
use App\Repository\CommonRepository;
use App\Services\XLSXWriter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Models\Activity;
use View;


/**
 * @property CommonRepository common
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /** @var User */
    protected $user;
    protected $table;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user   = Auth::user();
            $this->common = app(CommonRepository::class);

            View::share('user', $this->user);

            return $next($request);
        });
    }

    /**
     * @param $url
     * @param $errors
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirectErrorWith($url, $errors)
    {
        return redirect($url)->with('errors', $errors);
    }

    /**
     * @param array|string $errors
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirectBackErrorWith($errors)
    {
        $errors = is_array($errors) ? $errors : [$errors];
        return back()->with('errors', $errors);
    }

    /**
     * @param $model
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirectShow($model)
    {
        return redirect(route("{$this->table}.show",$model));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirectIndex()
    {
        return redirect(route("{$this->table}.index"));
    }

    /**
     * @param Model|Material|RegEvent|Inspector $model
     * @param $name
     * @return array
     */
    protected function uploadFiles(Model $model, $name)
    {
        $files = $model->images?:[];

        if (request()->hasFile($name)) {

            /** @var UploadedFile $file */
            foreach (request()->file($name) as $file) {
                $imageName = $file->getFilename() . str_random() . '.' . $file->getClientOriginalExtension();
                $file->move(base_path() . "/public/i/uploads/{$this->table}", $imageName);
                //\Image::make($file)->resize(300, 300)->save(base_path() . "/public/i/uploads/{$this->table}/$imageName");
                $files[] = $imageName;
            }
        }

        return $files;
    }

    /**
     * @param Model|Material|RegEvent $model
     * @param array $files
     * @return array
     */
    protected function removeFiles(Model $model, array $files)
    {
        if ($files) {
            $deleted = false;
            $images = $model->images;

            foreach ($files as $file) {
                if (!$file) continue;

                if (($key = array_search($file, $images)) !== false) {
                    unset($images[$key]);

                    $path = base_path() . "/public/i/uploads/{$this->table}/$file";
                    if (file_exists($path)) {
                        unlink($path);
                    }

                    $deleted = true;
                }
            }

            if ($deleted) {
                $model->images = $images;
                $model->save();

                return ['message' => 'Успешно удалено'];
            }
        }

        return ['error' => 'Что-то пошло не так, попробуйте позже'];
    }

    /**
     * @param Model|Inspector|Material|RegEvent $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setReadonly(Model $model)
    {
        try {
            if (!$this->user->hasRole('admin'))
                throw new AdminException('У вас недостаточно прав');

            $model->toogleReadonly();

            return $this->redirectIndex()->with('success', __('messages.readonly.'.intval($model->readonly)));
        } catch (AdminException $e) {
            return $this->redirectBackErrorWith([$e->getMessage()]);
        }
    }

    /**
     * Выгрузка в Excel
     * @param array $columns
     * @param Builder $query
     */
    public function excelGenerate(array $columns, Builder $query)
    {
        $writer = new XLSXWriter();
        $limit  = 200;
        $formatted = [];
        $widths    = [];

        foreach ($columns as $key => $value) {
            $name = trans("cp.$value");
            if ($name == "cp.$value") {
                $name = $value;
            }
            $formatted[$name] = 'string';
            $widths[$name] = 20;
        }

        $writer->writeSheetHeader('Sheet1', $formatted, $col_options = ['auto_filter'=>true, 'widths' => $widths]);
        $currentQueryCount = $query->count();

        for ($offset = 0; $offset < $currentQueryCount; $offset+= $limit) {
            $data = $query->offset($offset)->limit($limit)->get();
            foreach ($data as $one) {
                $onerow = $one->only($columns);
                $writer->writeSheetRow('Sheet1', $onerow);
            }
        }

        $filename = "OK_{$this->table}" . date('__Ymd_His') . '.xlsx';
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename=' . $filename);
        header('Cache-Control: max-age=0');
        return $writer->writeToStdOut();
    }

    public function getAudit(Model $model)
    {
        $all = Activity::where('subject_type', get_class($model))
            ->where('subject_id', $model->id)
            ->orderBy('id', 'desc')
            ->get();

        return view('cp.audit.show', [
            'activites' => $all,
            'model' => $model
        ]);
    }

}
