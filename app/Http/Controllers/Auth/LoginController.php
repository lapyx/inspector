<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ProfileUpdateRequest;

use App\Models\Session;
use App\Models\User;

use App\Repository\UserRepository;

use Illuminate\Support\Facades\Auth;

use Route, Log;

class LoginController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        if (Auth::user())
            return redirect('/');

        return  view('auth.login');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function login(Request $request)
    {
        $req_user = $request->only('email', 'password');

        $user = User::where('email',$req_user['email']) -> first();

        if ($user && Auth::attempt($req_user, request('remember'))) {
            $this->auth($user);
            return redirect(config('app.url'));
        } else {
            return $this->redirectBackErrorWith('Неправильный логин или пароль');
        }
    }

    /**
     * @param User $user
     */
    private function auth(User $user)
    {
        // Save session
        $session = app(Session::class)->newInstance([
            'token'      => request()->session()->getId(),
            'user_agent' => request()->header('user-agent'),
            'remote_ip'  => request()->ip()
        ]);
        $user->sessions()->save($session);
        $user->save();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout(Request $request)
    {
        Auth::guard()->logout();
        $request->session()->invalidate();

        return redirect()->route('home');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile()
    {
        $routes = $this->user->getRoutesByPermission();
        return view('auth.profile', compact('routes'));
    }

    /**
     * @param ProfileUpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function profileUpdate(ProfileUpdateRequest $request)
    {
        $data = $request->only(array_keys($request->rules()));

        if ($data['password']) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }

        $this->user->update($data);
        return redirect()->route('profile')->with('success', __('messages.update'));
    }

}
