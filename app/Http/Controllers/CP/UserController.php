<?php

namespace App\Http\Controllers\CP;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserCreateRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Models\User;
use Yajra\Datatables\Datatables;

class UserController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cp.users.index');
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function anyData()
    {
        return Datatables::of(User::query())
            ->addColumn('action', function (User $user) {
                return view('cp.parts.actions', ['controller' => 'users', 'object' => $user, 'action' => 'table']);
            })
            ->addColumn('roles', function (User $user) {
                return $user->getNameRoles();
            })
            ->addColumn('regions', function (User $user) {
                return $user->getRegionsName();
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cp.users.create', [
            'regions' => $this->common->getRegionsByUser($this->user)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserCreateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        $fields = array_keys($request->rules());
        $data   = $request->only($fields);
        $data['password'] = bcrypt($data['password']);

        $user   = User::create($data);
        $user->regions()->sync($request->input('regions'));
        $user->roles()->sync($request->input('roles'));
        $user->permissions()->sync($request->input('permissions'));

        return redirect()->action('CP\UserController@show', $user)->with('success', __('messages.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('cp.users.show', ['userEdit' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('cp.users.edit', [
            'userEdit' => $user,
            'regions' => $this->common->getRegionsByUser($this->user)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserUpdateRequest $request
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        $fields = array_keys($request->rules());
        $data   = $request->only($fields);

        if ($data['password']) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }

        $user->update($data);
        $user->regions()->sync($request->input('regions'));
        $user->roles()->sync($request->input('roles'));
        $user->permissions()->sync($request->input('permissions'));

        return redirect()->action('CP\UserController@show', $user)->with('success', __('messages.update'));
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->action('CP\UserController@index')->with('success', __('messages.delete'));
    }
}
