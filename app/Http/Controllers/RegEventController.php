<?php

namespace App\Http\Controllers;

use App\Exceptions\AdminException;
use App\Http\Requests\RegEventCreateRequest;
use App\Models\RegEvent;
use App\Repository\InspectorRepository;
use App\Repository\RegEventRepository;
use DataTables, DB;

/**
 * Class RegEventController
 * @package App\Http\Controllers
 */
class RegEventController extends Controller
{
    protected $table = 'reg_events';
    protected $inspectors;
    protected $regevents;

    /**
     * MaterialController constructor.
     * @param InspectorRepository $inspectors
     * @param RegEventRepository $regevents
     */
    public function __construct(InspectorRepository $inspectors, RegEventRepository $regevents)
    {
        parent::__construct();

        $this->inspectors = $inspectors;
        $this->regevents  = $regevents;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cp.parts.index', $this->datatableParams());
    }

    public function datatableParams()
    {
        return [
            'table' => $this->table,
            'url' => route("{$this->table}.datatables.data"),
            'columns' => [
                [ 'data' => 'id', 'name' => 'id' ],
                [ 'data' => 'name', 'name' => 'name' ],
                [ 'data' => 'inspector', 'name' => 'inspector' ],
                [ 'data' => 'region_name', 'name' => 'region_name' ],
                [ 'data' => 'is_joint', 'name' => 'is_joint' ],
                [ 'data' => 'count_warnings', 'name' => 'count_warnings' ],
                [ 'data' => 'sum_penaltys', 'name' => 'sum_penaltys' ],
                [ 'data' => 'date_event', 'name' => 'date_event' ],
                [ 'data' => 'action', 'name' => 'action', 'orderable'=> false, 'searchable'=> false]
            ]
        ];
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function anyData()
    {
        $query = $this->regevents->queryByUser($this->user, ['reg_events.*', 'regions.name as region_name']);

        return DataTables::of($query)
            ->filterColumn('region_name', function($query, $keyword) {
                $query->whereRaw("regions.name like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('inspector', function($query, $keyword) {
                $query->whereRaw("CONCAT_WS(' ', nam, fam, otch) like ?", ["%{$keyword}%"]);
            })
            ->addColumn('action', function ($model) {
                return view('cp.parts.actions', ['controller'=>$this->table, 'object' => $model, 'action'=>'table']);
            })
            ->addColumn('id', function (RegEvent $model) {
                return '<a href="'.route("{$this->table}.show", $model).'" class="btn btn-info btn-xs" title="Просмотр">'.$model->id.'</a>';
            })
            ->addColumn('inspector', function (RegEvent $model) {
                return $model->inspector->getAhref();
            })
            ->addColumn('is_joint', function (RegEvent $model) {
                return $model->is_joint ? 'Да' : 'Нет';
            })
            ->editColumn('date_event', function (RegEvent $model) {
                return $model->date_event;
            })
            ->rawColumns(['id', 'action', 'inspector'])
            ->make(true);
    }


    /**
     * @param RegEvent $reg_event
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show(RegEvent $reg_event)
    {
        return view("cp.{$this->table}.show", ['model' => $reg_event]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        return $this->edit();
    }

    /**
     * @param RegEventCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RegEventCreateRequest $request)
    {
        try {
            $data = $request->only(array_keys($request->rules()));

            $reg_event = new RegEvent();

            $data['images'] = $this->uploadFiles($reg_event, 'images');

            $reg_event = $reg_event->newInstance();
            $reg_event->fill($data);
            $reg_event->save();

            return $this->redirectShow($reg_event)->with('success', __('messages.create'));
        } catch (AdminException $e) {
            return $this->redirectBackErrorWith([$e->getMessage()]);
        }
    }

    /**
     * @param RegEvent $reg_event
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit(RegEvent $reg_event = null)
    {
        $params = [];
        $params['inspectors'] = $this->inspectors->getByRegion($this->user->getRegionsId());
        $params['regions']    = $this->common->getRegionsByUser($this->user);

        if ($reg_event) {
            if (!$reg_event->allowed($this->user, 'change'))
                return $this->redirectBackErrorWith('У вас недостаточно прав');

            return view("cp.{$this->table}.edit", ['model' => $reg_event] + $params);
        } else {
            return view("cp.{$this->table}.create", $params);
        }
    }

    /**
     * @param RegEventCreateRequest $request
     * @param RegEvent $reg_event
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(RegEventCreateRequest $request, RegEvent $reg_event)
    {
        try {
            if (! $reg_event->allowed($this->user, 'change'))
                throw new AdminException('У вас недостаточно прав');

            $data = $request->only(array_keys($request->rules()));

            $data['images'] = $this->uploadFiles($reg_event, 'images');

            $reg_event->update($data);

            return $this->redirectShow($reg_event)->with('success', __('messages.update'));
        } catch (AdminException $e) {
            return $this->redirectBackErrorWith([$e->getMessage()]);
        }
    }

    /**
     * @param RegEvent $reg_event
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(RegEvent $reg_event)
    {
        try {
            if (! $reg_event->allowed($this->user, 'delete'))
                throw new AdminException('У вас недостаточно прав');

            $reg_event->delete();

            return $this->redirectIndex()->with('success', __('messages.delete'));
        } catch (AdminException $e) {
            return $this->redirectBackErrorWith([$e->getMessage()]);
        }
    }

    /**
     * @param RegEvent $reg_event
     * @return array
     */
    public function deleteFile(RegEvent $reg_event)
    {
        $file = basename(request('file'));

        return $this->removeFiles($reg_event, [$file]);
    }

    /**
     * @param RegEvent $regEvent
     * @return \Illuminate\Http\RedirectResponse
     */
    public function readonly(RegEvent $regEvent)
    {
        return $this->setReadonly($regEvent);
    }

    /**
     * выгрузка в Excel
     */
    public function getExcel()
    {
        $columns = \Schema::getColumnListing($this->table);
        // удаляем лишние колонки
        $columns = array_unset_values($columns, ['images']);

        $query = $this->regevents->queryByUser($this->user, [
            'reg_events.*',
            DB::raw("CONCAT_WS(' ', nam, fam, otch) as inspector_id"),
            'regions.name as region_id',
            DB::raw("IF(reg_events.readonly, 'Да', 'Нет') as readonly"),
            DB::raw("IF(is_joint, 'Да', 'Нет') as is_joint"),
            DB::raw("IF(is_control, 'Да', 'Нет') as is_control"),
        ]);

        if ($this->user->hasRole('admin')) {
            $query = $query->withTrashed();
        }

        return $this->excelGenerate($columns, $query);
    }

    /**
     * @param RegEvent $regEvent
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function audit(RegEvent $regEvent)
    {
        return $this->getAudit($regEvent);
    }

}
