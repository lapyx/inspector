<?php

namespace App\Repository;

use App\Models\Region;
use App\Models\Thematic;
use App\Models\User;
use App\Traits\RepositoryTrait;
use Illuminate\Database\Eloquent\Model;

use Log;
/**
 * Class CommonRepository
 *
 * @package App\Repository
 */
class CommonRepository
{
    use RepositoryTrait;

    /**
     * @param Model $entity
     */
    public function loadEntity(Model $entity)
    {
        $this->entity = $entity;
    }

    /**
     * @param User $user
     * @return \Eloquent[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getRegionsByUser(User $user)
    {
        $this->loadEntity(new Region);

        $query = $this->entity;

        if ($regionsId = $user->getRegionsId()) {
            $query = $query->whereIn('id', $regionsId);
        }

        return $query->orderBy('code', 'asc')->get();
    }

    /**
     * Добавляет новую тематику в список или вытаскивает существующий
     * при успехе возвращает id иначе null
     * @param $value
     * @return int|null
     */
    public function addThematic($value)
    {
        $thematic = null;

        if ($value) {
            if (is_numeric($value)) {
                $thematic = Thematic::find($value);
            } else {
                $thematic = Thematic::create(['name' => $value]);
            }
        }

        if ($thematic) {
            return $thematic->id;
        }

        return null;
    }

}
