<?php

namespace App\Repository;

use App\Models\Material;
use App\Models\User;
use App\Traits\RepositoryTrait;

use Log;
/**
 * Class MaterialRepository
 *
 * @package App\Repository
 */
class MaterialRepository
{
    use RepositoryTrait;

    /**
     * @param Material $entity
     */
    public function __construct(Material $entity)
    {
        $this->entity = $entity;
    }

    /**
     * @param $ids
     * @return RegEvent[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getByRegion($ids)
    {
        $query = $this->entity;

        if ($ids) {
            $query = $query->whereIn('region_id', $ids);
        }

        return $query->select('*')
            ->orderBy('name')
            ->get();
    }

    /**
     * @param User $user
     * @param $select
     * @return $this|\Illuminate\Database\Query\Builder|static
     */
    public function queryByUser(User $user, $select = '*')
    {
        $query = $this->entity->select($select)
            ->join('inspectors', 'inspectors.id', '=', 'inspector_id')
            ->join('regions', 'regions.id', '=', 'materials.region_id')
            ->leftJoin('thematics', 'thematics.id', '=', 'thematic_id')
            ->leftJoin('reg_events', 'reg_events.id', '=', 'reg_event_id');


        if (!$user->hasRole('admin') && $user->regions->count()) {
            $query = $query->whereIn('materials.region_id', $user->getRegionsId());
        }

        return $query;
    }
    
}
