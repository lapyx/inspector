<?php

namespace App\Repository;

use App\Models\User;
use App\Traits\RepositoryTrait;

use Log;
/**
 * Class UserRepository
 *
 * @package App\Repository
 */
class UserRepository
{
    use RepositoryTrait;

    /**
     * @param User $entity
     */
    public function __construct(User $entity)
    {
        $this->entity = $entity;
    }

     public function findByEmail($email)
     {
         return $this->entity->where('email', $email)->first();
     }

//     public function createUser($name, $dn, $depart = null, $phone=null, $email = null, $role_id = null, $filials = [])
//     {
//         if (count($filials) == app(Filial::class)->count()) {
//             $filials = null;
//         }
//
//         $user = $this->getNew([
//             'name'               => $name,
//             'dn'                 => $dn,
//             'password'           => '',
//             'email'              => $email ? $email : null,
//             'depart'             => $depart ? $depart : null,
//             'phone'              => $phone ? $phone : null,
//             'role_id'            => $role_id ? $role_id : RoleType::GUEST,
//             'permission_filials' => $filials
//         ]);
//
//         $this->save($user);
//
//         return $user;
//     }
}
