<?php

namespace App\Repository;

use App\Models\RegEvent;
use App\Models\User;
use App\Traits\RepositoryTrait;

use Log;

/**
 * Class RegEventRepository
 *
 * @package App\Repository
 */
class RegEventRepository
{
    use RepositoryTrait;

    /**
     * @param RegEvent $entity
     */
    public function __construct(RegEvent $entity)
    {
        $this->entity = $entity;
    }

    /**
     * @param $ids
     * @return RegEvent[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getByRegion($ids)
    {
        $query = $this->entity;

        if ($ids) {
            $query = $query->whereIn('region_id', $ids);
        }

        return $query->select('reg_events.*')
            ->orderBy('name')
            ->get();
    }

    /**
     * @param User $user
     * @param string $select
     * @return $this|\Illuminate\Database\Eloquent\Builder
     */
    public function queryByUser(User $user, $select = '*')
    {
        /** @var \Illuminate\Database\Eloquent\Builder $query */
        $query = $this->entity->select($select)
            ->join('inspectors', 'inspectors.id', '=', 'inspector_id')
            ->join('regions', 'regions.id', '=', 'reg_events.region_id');

        if (!$user->hasRole('admin') && $user->regions->count()) {
            $query = $query->whereIn('reg_events.region_id', $user->getRegionsId());
        }

        return $query;
    }

}
