<?php

namespace App\Repository;

use App\Models\Inspector;
use App\Models\User;
use App\Traits\RepositoryTrait;

use Log;
/**
 * Class InspectorRepository
 *
 * @package App\Repository
 */
class InspectorRepository
{
    use RepositoryTrait;
    protected $user;

    /**
     * @param Inspector $entity
     */
    public function __construct(Inspector $entity)
    {
        $this->entity = $entity;
        $this->user = \Auth::user();
    }

    /**
     * @param $ids
     * @return Inspector[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getByRegion($ids)
    {
        $query = $this->entity;

        if ($ids) {
            $query = $query->whereIn('region_id', $ids);
        }

        return $query->with('region')->orderBy(\DB::raw('fam, nam, otch'))->get();
    }

    /**
     * @param User $user
     * @param $select
     * @return $this|\Illuminate\Database\Query\Builder|static
     */
    public function queryByUser(User $user, $select)
    {
        $query = $this->entity->select($select)
            ->join('regions', 'regions.id', '=', 'region_id')
            ->leftJoin('users', 'users.id', '=', 'inspectors.user_id');

        if (!$user->hasRole('admin') && $user->regions->count()) {
            $query = $query->whereIn('region_id', $user->getRegionsId());
        }

        return $query;
    }

}
