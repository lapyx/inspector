<?php

namespace App\Jobs;

use App\Enum\ActionType;
use App\Models\Alien;
use App\Models\Bank;
use App\Models\Mib\MibSession;
use App\Models\User;
use App\Models\Order;
use App\Repository\ActionRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Swift_Mailer;
use Log;

class SendEmailJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $bank;
    public $alien;
    public $user;
    public $order;
    public $text;
    public $data;
    public $view;
    public $params;
    public $subject;
    public $files;
    public $mibsession;


    /**
     * SendEmailJob constructor.
     * @param Bank $bank
     * @param $recipient
     * @param Order|null $order
     * @param $subject
     * @param $view
     * @param array $params
     * @param array $files
     * @param MibSession|null $mibsession
     */
    public function __construct(Bank $bank, $recipient, Order $order = null, $subject, $view, array $params = [], array $files = [], MibSession $mibsession = null)
    {
        if ($recipient instanceof Alien){
            $this->alien = $recipient;
        }
        elseif ($recipient instanceof User){
            $this->user = $recipient;
        }
        else
            throw new Exception("recipient is not class App\Models\Alien or App\Models\User");

        $this->bank        = $bank;
        $this->order       = $order;
        $this->view        = $view;
        $this->files       = $files;
        $this->mibsession  = $mibsession;
        $params_only_scalar = [];
        foreach ($params as $key => $value) {
            if (is_scalar($value)){
                $params_only_scalar[$key] = $value;
            }
        }
        $this->subject = trans($subject, $params_only_scalar);

        $data = [
            'alien'        => $this->alien,
            'user'         => $this->user,
            'order'        => $this->order,
            'is_test_base' => false
        ];

        if ($params) {
            if (is_array($params)) {
                $data = array_merge($data, $params);
            }

            if ($this->user && env('APP_ENV') != 'prod') {
                $this->subject = 'ТЕСТОВАЯ БАЗА!!! '.$this->subject;
                $data['is_test_base'] = true;
            }

            $params['subject'] = $this->subject;
            $params['view'] = $this->view;

            $this->params = $params;
        }

        $this->data = $data;
    }


    /**
     * @param ActionRepository $actions
     * @return bool
     */
    public function handle(ActionRepository $actions)
    {
        $reception = $this->alien ? $this->alien : $this->user;

        if(!$reception->email) {
            Log::debug('reception ID: {'.$reception->id.'} is empty email');
            return false;
        }

        $bank    = $this->bank;
        $subject = $this->subject;
        $files   = $this->files;
        Mail::send($this->view, $this->data, function ($message) use ($reception, $bank, $subject, $files) {
            if ($files) {
                foreach ($files as $onefile) {
                    Log::info('Attach file email: '.$onefile['name']);
                    $message->attachData(base64_decode($onefile['content']), $onefile['name']);
                }
            }
            $message->from( $bank->mail_from_addr(), $bank->mailfrom_name() )
                ->to($reception->email, $reception->name)
                ->subject($subject)
            ;
        });

        $result = NULL; // TODO id message
        if ($this->alien) {
            $actions->setForAlien(ActionType::EMAIL, $bank->id, $this->alien, $subject, $this->order, $result, null, $this->params);
        } else {
            $actions->setForUser(ActionType::EMAIL, $this->user, $subject, $this->order, $result, null, $this->params, $this->mibsession);
        }
        /*if ( config('queue.default') !='sync' ){
            echo 'mail '.$bank->alias.': <'.$bank->mail_from_addr.'> => <'.$reception->email.'> ['.$subject.']'.PHP_EOL;
        }*/
        Log::info('mail '.$bank->alias().': <'.$bank->mail_from_addr().'> => <'.$reception->name.' ('. $reception->email.')> ['.$subject.']');
    }

}
