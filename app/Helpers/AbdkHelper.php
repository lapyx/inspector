<?php

use Carbon\Carbon;

if (! function_exists('dateString')) {

    /**
     * @param Carbon $date
     * @return Carbon|string
     */
    function dateString(Carbon $date)
    {
        $dmy = $date->format('d.m.Y');
        $hi  = $date->format('H:i');

        if ($dmy == Carbon::now()->format('d.m.Y')) {
            $date = 'Сегодня, '.$hi;
        } elseif($dmy == Carbon::yesterday()->format('d.m.Y')) {
            $date = 'Вчера, '.$hi;
        } else {
            $date = $dmy.' '.$hi;
        }
        return $date;
    }
}

if (! function_exists('is_array_object')) {
    /**
     * @param $arr
     * @return bool
     */
    function is_array_object($arr)
    {
        return is_array($arr) && is_string(key($arr));
    }
}

if (! function_exists('is_array_arrayobject')) {
    /**
     * @param $arr
     * @return bool
     */
    function is_array_arrayobject($arr)
    {
        return is_array($arr) && is_integer(key($arr)) && is_array_object(current($arr));
    }
}

if (! function_exists('getUrlPrefix')) {
    /**
     * @return mixed
     */
    function getUrlPrefix()
    {
        return env('APP_URL_PREFIX');
    }
}

if (! function_exists('getExtensionFile')) {
    /**
     * @param $file
     * @return mixed
     */
    function getExtensionFile($file)
    {
        $ext = pathinfo($file);
        return strtolower(array_get($ext, 'extension'));
    }
}

if (! function_exists('getImgFancybox')) {
    /**
     * @param $url
     * @param string $w
     * @param string $h
     * @return mixed
     */
    function getImgFancybox($url, $w='130', $h='130')
    {
        $ext = getExtensionFile($url);
        switch ($ext) {
            case 'pdf':
                $img = '<i class="fa fa-file-pdf-o"></i>';
                break;
            case 'zip':
            case 'rar':
                $img = '<i class="fa fa-file-archive-o"></i>';
                break;
            default:
                $img = '<img src="'.$url.'" class="img-rounded" width="'.$w.'" height="'.$h.'"/>';
        }

        return '<a rel="example_group" href="'.$url.'" class="fancybox">'.$img.'</a>';
    }
}


if (! function_exists('norm_phone') ){
    /**
     * @param $value
     * @return bool|string|string[]|null
     */
    function norm_phone($value)
    {
        $value  = preg_replace('/[^0-9]/','', $value);
        if (strlen($value)==11 && (substr($value, 0,1)=='7' || substr($value, 0,1)=='8') ){
            $value  = substr($value , 1);
        }
        return $value;
    }
}


if (! function_exists('is_phone_cool') ){
    /**
     * @param $value
     * @param int $num
     * @return bool
     */
    function is_phone_cool($value, $num=10)
    {
        return preg_match('/^(\+?7|8|)\d{'.$num.'}$/', $value);
    }
}

if (! function_exists('array_unset_values') ) {
    /**
     * Удаляет из массива указанные значения
     * @param array $array
     * @param array $values
     * @return array
     */
    function array_unset_values(array $array, array $values = [])
    {
        return array_where($array, function ($value) use ($values) {
            return !in_array($value, $values);
        });
    }
}

if (! function_exists('array_unset_keys') ) {
    /**
     * Удаляет из массива указанные значения
     * @param array $array
     * @param array $values
     * @return array
     */
    function array_unset_keys(array $array, array $values = [])
    {
        return array_where($array, function ($value, $key) use ($values) {
            return !in_array($key, $values);
        });
    }
}

if (! function_exists('array_swap_values') ) {
    /**
     * Меняет значение массива на другой не меняя его положения в массиве
     * @param array $array
     * @param array $swaps
     * @return array
     */
    function array_swap_values(array $array, array $swaps = [])
    {
        foreach ($swaps as $currentKey => $changeKey)
        {
            $index = array_search($currentKey, $array);
            if ($index !== false) {
                $array[$index] = $changeKey;
            }
        }
        return $array;
    }
}

function convert_fields_by_model_name(\Spatie\Activitylog\Models\Activity $activity, $array)
{
    foreach ($array as $field => $one) {
        if ($array[$field]) {
            switch ($field) {
                case 'region_id' :
                    $array[$field] = \App\Models\Region::find($array[$field])->name;
                    break;
                case 'user_id' :
                    $array[$field] = \App\Models\User::find($array[$field])->username;
                    break;
                case 'reg_event_id' :
                    $array[$field] = \App\Models\RegEvent::withTrashed()->find($array[$field])->name;
                    break;
                case 'inspector_id' :
                    $array[$field] = \App\Models\Inspector::withTrashed()->find($array[$field])->socrName();
                    break;
                case 'thematic_id' :
                    $array[$field] = \App\Models\Thematic::find($array[$field])->name;
                    break;
                case 'images' :
                    $model = app($activity->subject_type)->find($activity->subject_id);
                    if ($model) {
                        $images = '';
                        foreach ($array[$field] as $file) {
                            $images .= $model->getImg($file).'&nbsp&nbsp&nbsp';
                        }
                        $array[$field] = $images;
                    }
                    break;
                case 'readonly' :
                case 'is_criminal' :
                case 'is_joint' :
                case 'is_control' :
                    $array[$field] = $array[$field] ? 'Да' : 'Нет';
                    break;
                default :
                    if (is_array($array[$field])) {
                        $array[$field] = implode(',', $array[$field]);
                    }
                    break;
            }
        }
    }

    return $array;
}
