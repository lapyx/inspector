<?php
namespace App\Services;

use \Illuminate\Routing\Router as Routing;
use Illuminate\Support\Facades\Route;

use Log;

/**
 * Example
 * автоматически создаст контроллер с методами resource и подцепит модель
 * php artisan make:controller PageController --resource --model=Page
 *
 * MyRoute::resourcePermission($permission, $controller)
 * функция сама строит готовые роуты к методам контроллера, пример:
 *
 * users.edit      App\Http\Controllers\CP\UserController@edit     GET    users/{user}/edit
 * users.update    App\Http\Controllers\CP\UserController@update   PUT    users/{user}
 * users.create    App\Http\Controllers\CP\UserController@create   GET    users/create
 * users.store     App\Http\Controllers\CP\UserController@store    POST   users
 * users.destroy   App\Http\Controllers\CP\UserController@destroy  DELETE users/{user}
 * users.index     App\Http\Controllers\CP\UserController@index    GET    users
 * users.show      App\Http\Controllers\CP\UserController@show     GET    users/{user}
 *
 * и добавляет к каждому роуту проверку на разрешения (permissions), пример
 *
 * Route::group(['middleware' => 'permission:users.show'],
 * Route::group(['middleware' => 'permission:users.edit'],
 * Route::group(['middleware' => 'permission:users.create'],
 * Route::group(['middleware' => 'permission:users.destroy'],
 *
 * $permission - подаем пермишен, который будет префиксом к действию, например
 *     есть пермишены users.show, users.edit и т.д. тогда надо подать просто users,
 *     а show,edit и т.д добавит функция и проверит есть ли такие пермишены у юзера
 * $controller - контроллер
 */

/** Example
  * MyRoute::checkPermission($permission, $params) функция для добавления дополнительных роутов к пермишенам
  * $permission - подаем пермишен, который будет префиксом к действию, которые задаем в $params, например
  *     есть пермишены site.pages.show, site.pages.edit и т.д. тогда надо подать site.pages,
  *     а show,edit надо прописать в $params и добавить в них нужные роуты
  * $params - массив действий permission => доступные роуты
  * show     - просмотр
  * edit     - редактирвоание
  * create   - создание
  * destroy  - удаление
  * ...other - любой другой пермишен
  * Пример:
  * MyRoute::checkPermission('site.pages', [
  *   permission                                routes
  *    'show'    => function() {
  *        Route::get('pages', ['as' => 'page_index',  'uses' => 'PageController@index']);
  *        Route::get('pages/{page}', ['as' => 'page_show',  'uses' => 'PageController@show']);
  *    },
  *    'edit'    => function () {
  *        Route::resource('pages', 'PageController', ['only' => ['edit','update']]);
  *    },
  *    'create'  => function() {
  *        Route::post('pages/create', ['as' => 'page_create',  'uses' => 'PageController@create']);
  *    },
  *    'destroy' => function() {
  *        Route::delete('pages/destroy/{page}', ['as' => 'page_destroy',  'uses' => 'PageController@destroy']);
  *    },
  *    'other' => function() {
  *        Route::get('pages/menu', ['as' => 'page_menu',  'uses' => 'PageController@menu']);
  *    }
  * ]);
  */


class Router extends Routing
{
    /**
     * Добавляет к Route::resource проверку прав доступа пользователя по middleware => permission
     *
     * @param $prefixperm
     * @param $controller
     * @param array $checkPermission
     */
    public function resourcePermission($prefixperm, $controller, array $checkPermission = [])
    {
        $exp = explode('.', $prefixperm);
        if (count($exp) > 1) {
            $action = array_pop($exp);
        } else {
            $action = $prefixperm;
        }

        $params = [
            'show'    => function() use ($action, $controller, $checkPermission) {
                // Дополнительные роуты к show
                if (isset($checkPermission['show']) && is_callable($checkPermission['show']))
                    $checkPermission['show']($action);

                // Просмотр
                Route::resource($action, $controller, ['only' => ['index','show']]);
            },
            'edit'    => function() use ($action, $controller, $checkPermission) {
                // Дополнительные роуты к edit
                if (isset($checkPermission['edit']) && is_callable($checkPermission['edit']))
                    $checkPermission['edit']($action);

                // Редактирование
                Route::resource($action, $controller, ['only' => ['edit','update']]);

            },
            'create'  => function() use ($action, $controller, $checkPermission) {
                // Дополнительные роуты к create
                if (isset($checkPermission['create']) && is_callable($checkPermission['create']))
                    $checkPermission['create']($action);

                // Добавление
                Route::resource($action, $controller, ['only' => ['create','store']]);
            },
            'destroy' => function() use ($action, $controller, $checkPermission) {
                // Дополнительные роуты к destroy
                if (isset($checkPermission['destroy']) && is_callable($checkPermission['destroy']))
                    $checkPermission['destroy']($action);

                // Удаление
                Route::resource($action, $controller, ['only' => ['destroy']]);
            }
        ];

        unset(
            $checkPermission['show'],
            $checkPermission['edit'],
            $checkPermission['create'],
            $checkPermission['destroy']
        );

        $params = array_merge($params, $checkPermission);

        $this->checkPermission($prefixperm, $params);
    }

    /**
     * @var string $prefixperm
     * @var array $params ([show, edit, create, destroy, ...add other])
     */
    public function checkPermission($prefixperm, array $params)
    {
        Route::group(['middleware' => 'permission:'.$prefixperm.'.show'], function () use ($prefixperm, $params)
        {
            foreach ($params as $action => $callfunc) {
                if ($action == 'show') continue; // должен срабатывать последним

                if (is_callable($callfunc)) {
                    Route::group(['middleware' => 'permission:'.$prefixperm.'.'.$action], function() use ($prefixperm, $callfunc) {
                        $callfunc($prefixperm);
                    });
                }
            }

            if (isset($params['show']) && is_callable($params['show']))
                $params['show']($prefixperm);
        });
    }
}
