<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;
/**
 * Наследует все методы класса Illuminate\Support\Facades\Route
 * @see App\Services\Router
 */
class MyRoute extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'my.router';
    }
}
