<?php

namespace App\Models\RBAC;

use Laratrust\LaratrustPermission;
use Spatie\Activitylog\Traits\LogsActivity;

class Permission extends LaratrustPermission {
    use LogsActivity;

    // Игнорирует логирование изменений полей
    protected static $ignoreChangedAttributes = ['created_at', 'updated_at'];

    // какие поля логируем
    protected static $logAttributes = ['*'];

    // Логируем только те поля, которые действительно изменились
    protected static $logOnlyDirty = true;

    protected $fillable = [
        'name', 'display_name', 'description',
    ];

    public function getGroupPermissions()
    {
        $groups = trans('permission.groups');
        $all = $this->orderBy('id', 'ASC')->get();
        foreach($all as $one) {
            $hasGroup = false;
            foreach($groups as $name => &$onegroup) {
                if (!isset($onegroup['lists'])) {
                    $onegroup = [
                        'name'  => $onegroup,
                        'lists' => []
                    ];
                }
                if (strpos($one->name, $name) === 0) {
                    array_push($onegroup['lists'], $one);
                    $hasGroup = true;
                    break;
                }
            }

            if (!$hasGroup) {
                array_push($groups['other']['lists'], $one);
            }
        }
        //echo'<pre>';print_r($groups);die;
        return $groups;
    }

}
