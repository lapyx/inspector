<?php

namespace App\Models\RBAC;

use Laratrust\LaratrustRole;
use Spatie\Activitylog\Traits\LogsActivity;

class Role extends LaratrustRole {
    use LogsActivity;

    // Игнорирует логирование изменений полей
    protected static $ignoreChangedAttributes = ['created_at', 'updated_at'];

    // какие поля логируем
    protected static $logAttributes = ['*'];

    // Логируем только те поля, которые действительно изменились
    protected static $logOnlyDirty = true;

    // на какое действие включаем логирование
    protected static $recordEvents = ['created', 'updated', 'deleted'];

    protected $fillable = [
        'name', 'display_name', 'description'
    ];
}
