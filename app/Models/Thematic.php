<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Thematic extends Model
{
    public $guarded = [];
    public $timestamps = false;
}
