<?php

namespace App\Models;

use App\Models\RBAC\Permission;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
use Spatie\Activitylog\Traits\LogsActivity;
use Route;

/**
 * Class User
 * @package App\Models
 * @mixin User
 */
class User extends Authenticatable {

    use LaratrustUserTrait;
    use Notifiable;
    use LogsActivity;

    // Игнорирует логирование изменений полей
    protected static $ignoreChangedAttributes = ['created_at', 'updated_at', 'remember_token'];

    // какие поля логируем
    protected static $logAttributes = ['*'];

    // Логируем только те поля, которые действительно изменились
    protected static $logOnlyDirty = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'region_id', 'password'
    ];

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sessions()
    {
        return $this->hasMany(Session::class);
    }

    /**
     * Many-to-Many relations with Permission.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function regions()
    {
        return $this->belongsToMany(Region::class, 'user_regions', 'user_id', 'region_id');
    }

    /**
     * @return string
     */
    public function socrName()
    {
        $arrname = explode(' ', $this->username);

        $lastname  = $arrname[0].' ';
        $firstname = isset($arrname[1]) ? mb_substr($arrname[1],0,1).'.' : '';
        $patrname  = isset($arrname[2]) ? mb_substr($arrname[2],0,1).'.' : '';

        return $lastname.$firstname.$patrname;
    }

    /**
     * @return array
     */
    public function getRoutesByPermission()
    {
        $routes = [];
        $routeCollection = Route::getRoutes();
        /** @var Permission[] $permissions */
        $permissions = $this->allPermissions();
        $roles = $this->roles;

        foreach ($routeCollection as $route) {
            $action = $route->getAction(); // Array with full controller info
            $middleware = array_get($action, 'middleware');

            if ($middleware) {
                $middleware = is_array($middleware) ? $middleware : [$middleware];
                $hasPermissionMiddleware = implode('##', $middleware);
                if (strstr($hasPermissionMiddleware, '##permission:') !== FALSE) {
                    foreach ($permissions as $permission) {
                        if (in_array('permission:'.$permission->name, $middleware)) {
                            $routes[$permission->display_name.' ('.$permission->name.')'][] = $route;
                            break;
                        }
                    }
                } else {
                    $routes['all'][] = $route;
                }
                if (strstr($hasPermissionMiddleware, '##role:') !== FALSE) {
                    foreach ($roles as $role) {
                        if (in_array('role:'.$role->name, $middleware)) {
                            $routes[$role->display_name.' ('.$role->name.')'][] = $route;
                            break;
                        }
                    }
                }
            } else {
                $routes['all'][] = $route;
            }
        }
        return $routes;
    }

    /**
     * @return string
     */
    public function getNameRoles()
    {
        return implode(', ', $this->roles->pluck('display_name', 'id')->toArray());
    }

    /**
     * @param string $delim
     * @return string
     */
    public function getRegionsName($delim = ', ')
    {
        return $this->regions->count() ? implode($delim, $this->regions->pluck('name')->toArray()) : 'Все';
    }

    /**
     * @return array
     */
    public function getRegionsId()
    {
        return $this->regions->pluck('id')->toArray();
    }

    /**
     * @param $region_id
     * @return bool
     */
    public function hasRegion($region_id)
    {
        $regionIds = $this->getRegionsId();
        return empty($regionIds) || in_array($region_id, $regionIds);
    }

}
