<?php

namespace App\Models;

use App\Traits\ConvertDatesTrait;
use App\Traits\ReadonlyTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\UploadedFile;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Inspector
 * @package App\Models
 * @mixin Inspector
 */
class Inspector extends Model
{
    use SoftDeletes;
    use LogsActivity;
    use ConvertDatesTrait;
    use ReadonlyTrait;

    // Игнорирует логирование изменений полей
    protected static $ignoreChangedAttributes = ['created_at', 'updated_at', 'remember_token'];

    // какие поля логируем
    protected static $logAttributes = ['*'];

    // Логируем только те поля, которые действительно изменились
    protected static $logOnlyDirty = true;

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'birthday',
        'date_statement1',
        'date_statement2',
        'date_statement3',
        'date_renewal1',
        'date_renewal2',
        'date_renewal3',
        'doc_date_begin',
        'doc_date_end',
        'date_end_authority',
        'date_statement_doc'
    ];

    protected $casts = [
        'images' => 'array'
    ];

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::saving(function(Inspector $inspector){
            if ($inspector->phone) {
                $inspector->phone = norm_phone($inspector->phone);
            }

            if (!$inspector->user_id && ($user = \Auth::user())) {
                $inspector->user_id = $user->id;
            }
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function materials()
    {
        return $this->hasMany(Material::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function regEvents()
    {
        return $this->hasMany(RegEvent::class);
    }

    /**
     * @return string
     */
    public function socrName()
    {
        return $this->fam.' '.mb_substr($this->nam,0,1).'.'.($this->otch ? ' '.mb_substr($this->otch,0,1).'.' : '');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param User $user
     * @param $action
     * @return bool
     */
    public function allowed(User $user, $action)
    {
        if ($user->hasRole('admin')) return true;

        if ($action == 'change' || $action == 'delete') {
            return !$this->readonly && $user->hasRegion($this->region_id) && (is_null($this->user_id) || $this->user_id == $user->id);
        }

        return false;
    }

    /**
     * @param $file
     * @return string
     */
    public function getImg($file)
    {
        $url = asset("i/uploads/inspectors/$file");
        return getImgFancybox($url);
    }

    public function uploadPhoto()
    {
        if (request()->hasFile('photo')) {
            $dir = base_path() . "/public/i/uploads/inspector_photos";

            if (!file_exists($dir)) mkdir($dir);

            /** @var UploadedFile $file */
            $file = request()->file('photo');
            $imageName = $file->getFilename() . str_random() . '.' . $file->getClientOriginalExtension();
            \Image::make($file)->resize(300, 400)->save("$dir/$imageName");

            $this->photo = $imageName;
        }

        return $this->photo;
    }

    /**
     * @return mixed
     */
    public function getPhotoImg()
    {
        if ($this->photo) {
            $url = asset("i/uploads/inspector_photos/{$this->photo}");
            return getImgFancybox($url, 150, 200);
        } else {
            $url = asset('i/Default-avatar.jpg');
            return getImgFancybox($url, 180, 180);
        }
    }

    /**
     * @return string
     */
    public function getAhref()
    {
        return '<a href="'.route('inspectors.show', $this).'">'.$this->socrName().'</a>';
    }

}
