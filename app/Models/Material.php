<?php

namespace App\Models;

use App\Traits\ConvertDatesTrait;
use App\Traits\ReadonlyTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Material
 * @package App\Models
 * @mixin Material
 */
class Material extends Model
{
    use SoftDeletes;
    use LogsActivity;
    use ConvertDatesTrait;
    use ReadonlyTrait;

    // Игнорирует логирование изменений полей
    protected static $ignoreChangedAttributes = ['created_at', 'updated_at', 'remember_token'];

    // какие поля логируем
    protected static $logAttributes = ['*'];

    // Логируем только те поля, которые действительно изменились
    protected static $logOnlyDirty = true;

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'date_receipt'
    ];

    protected $guarded = [];


    protected $casts = [
        'images' => 'array'
    ];

    protected $attributes = [
        'count_warnings' => 0,
        'sum_penaltys' => 0,
        'count_protocols' => 0,
        'count_cancel_protocols' => 0,
        'confirmeted_warnings' => 0
    ];

    protected static function boot()
    {
        parent::boot();

        static::saving(function(Material $material){
            $material->count_warnings         = intval($material->count_warnings);
            $material->sum_penaltys           = intval($material->sum_penaltys);
            $material->count_protocols        = intval($material->count_protocols);
            $material->count_cancel_protocols = intval($material->count_cancel_protocols);
            $material->confirmeted_warnings   = intval($material->confirmeted_warnings);
            return $material;
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function inspector()
    {
        return $this->belongsTo(Inspector::class)->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function thematic()
    {
        return $this->belongsTo(Thematic::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function regEvent()
    {
        return $this->belongsTo(RegEvent::class)->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function confirmation()
    {
        return $this->belongsTo(Confirmation::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    /**
     * @param $file
     * @return string
     */
    public function getImg($file)
    {
        $url = asset("i/uploads/materials/$file");
        return getImgFancybox($url);
    }

    /**
     * @param User $user
     * @param $action
     * @return bool
     */
    public function allowed(User $user, $action)
    {
        if ($user->hasRole('admin')) return true;

        if ($action == 'change' || $action == 'delete') {
            return !$this->readonly && $user->hasRegion($this->inspector->region_id);
        }

        return false;
    }

    /**
     * @return string
     */
    public function getAhref()
    {
        return '<a href="'.route('materials.show', $this).'">Материал #'.$this->id.' ('.$this->inspector->socrName().')</a>';
    }

}
