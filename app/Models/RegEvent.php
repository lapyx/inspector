<?php

namespace App\Models;

use App\Traits\ConvertDatesTrait;
use App\Traits\ReadonlyTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class RegEvent
 * @package App\Models
 * @mixin RegEvent
 */
class RegEvent extends Model
{
    use SoftDeletes;
    use LogsActivity;
    use ConvertDatesTrait;
    use ReadonlyTrait;

    // Игнорирует логирование изменений полей
    protected static $ignoreChangedAttributes = ['created_at', 'updated_at', 'remember_token'];

    // какие поля логируем
    protected static $logAttributes = ['*'];

    // Логируем только те поля, которые действительно изменились
    protected static $logOnlyDirty = true;

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'date_event'
    ];

    protected $guarded = [];

    protected $casts = [
        'images' => 'array',
    ];

    protected $attributes = [
        'count_warnings' => 0,
        'sum_penaltys' => 0,
    ];

    protected static function boot()
    {
        parent::boot();

        static::saving(function(RegEvent $regEvent){
            $regEvent->count_warnings = intval($regEvent->count_warnings);
            $regEvent->sum_penaltys   = intval($regEvent->sum_penaltys);
            return $regEvent;
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function inspector()
    {
        return $this->belongsTo(Inspector::class)->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function materials()
    {
        return $this->hasMany(Material::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    /**
     * @param $img
     * @return string
     */
    public function getImg($img)
    {
        $url = asset("i/uploads/reg_events/$img");
        return getImgFancybox($url);
    }

    /**
     * @param User $user
     * @param $action
     * @return bool
     */
    public function allowed(User $user, $action)
    {
        if ($user->hasRole('admin')) return true;

        if ($action == 'change' || $action == 'delete') {
            return !$this->readonly &&  $user->hasRegion($this->inspector->region_id);
        }

        return false;
    }

    /**
     * @return string
     */
    public function getAhref()
    {
        return '<a href="'.route('reg_events.show', $this).'">'.$this->name.'</a>';
    }

}
