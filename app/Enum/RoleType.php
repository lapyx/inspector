<?php

namespace App\Enum;
use App\Models\RBAC\Role;

/**
 * Class
 * @package App\Enum
 */
class RoleType extends Enum
{
    const ADMIN       = 1; // Администратор
    const GUEST       = 2; // Гость
    const REGISTRAR   = 3; // Регистратор
    const ANALYST     = 4; // Аналитик

}
