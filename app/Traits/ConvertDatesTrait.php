<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ConvertDatesTrait
 * @mixin Model
 */
trait ConvertDatesTrait
{
    protected $convertDateFormat = 'd.m.Y';
    protected $dateTimeFormat = ['created_at', 'updated_at', 'deleted_at'];

    public function getAttribute($key)
    {
        $value = parent::getAttribute($key);

        if($this->isDateAttribute($key) && $value) {
            $format = $this->convertDateFormat;
            if (in_array($key, $this->dateTimeFormat)) {
                $format = $format.' H:i:s';
            }
            $value = $value->format($format);
        }

        return $value;
    }

    public function setAttribute($key, $value)
    {
        if($this->isDateAttribute($key) && $value) {
            $format = 'Y-m-d';
            if (in_array($key, $this->dateTimeFormat)) {
                $format = $format.' H:i:s';
            }
            $value = Carbon::parse($value)->format($format);
        }

        parent::setAttribute($key, $value);
    }


}