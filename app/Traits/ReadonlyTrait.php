<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ReadonlyTrait
 * @mixin Model
 */
trait ReadonlyTrait
{
    /**
     * блокирует/разблокирует запись для редактирования
     */
    public function toogleReadonly()
    {
        $this->readonly = $this->readonly ? 0 : 1;
        $this->save();
    }

}