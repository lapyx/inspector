<?php

namespace App\Providers;

use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Log, URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Schema::defaultStringLength(255);

        Validator::extendImplicit('key_exists', function ($attribute, $value, $parameters, $validator) {
            $data  = $validator->getData();
            return array_has($data, $attribute);
        });

        Validator::extendImplicit('is_array', function ($attribute, $value, $parameters, $validator) {
            //$data  = $validator->getData();
            return is_array($value);
        });

        Validator::extend('phone', function ($attribute, $value, $parameters, $validator) {
            return is_phone_cool($value);
        });

        //URL::forceScheme(env('APP_PROTOCOL', 'http'));

        //URL::forceRootUrl('/admin/');

        //$_SERVER['HTTP_HOST'] = config('app.url');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(IdeHelperServiceProvider::class);
        }
    }
}
